# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 192.168.0.33 (MySQL 5.5.38-0ubuntu0.14.04.1)
# Database: tappr_reg
# Generation Time: 2014-10-08 01:23:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table additional_partner_data
# ------------------------------------------------------------

CREATE TABLE `additional_partner_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `DOB` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) DEFAULT NULL,
  `PARTNER_STREET` varchar(255) DEFAULT NULL,
  `PARTNER_HOUSE_NUMBER` varchar(255) DEFAULT NULL,
  `PARTNER_POSTCODE` varchar(255) DEFAULT NULL,
  `PARTNER_CITY` varchar(255) DEFAULT NULL,
  `PARTNER_STATE` varchar(255) DEFAULT NULL,
  `PARTNER_NR` varchar(255) DEFAULT NULL,
  `MERCHANT_ID` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `MERCHANT_ID` (`MERCHANT_ID`),
  CONSTRAINT `additional_partner_data_ibfk_1` FOREIGN KEY (`MERCHANT_ID`) REFERENCES `merchant_data_card` (`MERCHANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table bank_account_details
# ------------------------------------------------------------

CREATE TABLE `bank_account_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `BANK_ACCOUNT_TYPE` varchar(255) DEFAULT NULL,
  `BANK_ACCOUNT_NAME` varchar(255) DEFAULT NULL,
  `BANK_ACCOUNT_NR` varchar(255) DEFAULT NULL,
  `BANK_ACCOUNT_BSB` varchar(255) DEFAULT NULL,
  `MERCHANT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `MERCHANT_ID` (`MERCHANT_ID`),
  CONSTRAINT `fk_bank_account` FOREIGN KEY (`MERCHANT_ID`) REFERENCES `merchant_data_card` (`MERCHANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table credit_card_details
# ------------------------------------------------------------

CREATE TABLE `credit_card_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `CREDIT_CARD_TYPE` varchar(255) DEFAULT NULL,
  `CREDIT_CARD_PAN` varchar(255) DEFAULT NULL,
  `CREDIT_CARD_EXPIRY` varchar(255) DEFAULT NULL,
  `MERCHANT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `MERCHANT_ID` (`MERCHANT_ID`),
  CONSTRAINT `fk_credit_card` FOREIGN KEY (`MERCHANT_ID`) REFERENCES `merchant_data_card` (`MERCHANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table merchant_data
# ------------------------------------------------------------

CREATE TABLE `merchant_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(255) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `BUSINESS_SERVICES` varchar(255) DEFAULT NULL,
  `BUSINESS_SUB_SERVICES` varchar(255) DEFAULT NULL,
  `BUSINESS_TYPE` varchar(255) DEFAULT NULL,
  `BUSINESS_NAME` varchar(255) DEFAULT NULL,
  `BUSINESS_ABN_ACN` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) DEFAULT NULL,
  `USER_AGENT` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `MERCHANT_ID` varchar(255) DEFAULT NULL,
  `PERSONAL_STREET` varchar(255) DEFAULT NULL,
  `PERSONAL_HOUSE_NUMBER` varchar(255) DEFAULT NULL,
  `PERSONAL_POSTCODE` varchar(255) DEFAULT NULL,
  `PERSONAL_CITY` varchar(255) DEFAULT NULL,
  `PERSONAL_STATE` varchar(255) DEFAULT NULL,
  `LOCATION_TYPE` varchar(255) DEFAULT NULL,
  `SEASONAL` varchar(255) DEFAULT NULL,
  `ANNUAL_TURNOVER` varchar(255) DEFAULT NULL,
  `AVERAGE_TRANSACTION` varchar(255) DEFAULT NULL,
  `KUNG_FU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

LOCK TABLES `merchant_data` WRITE;
/*!40000 ALTER TABLE `merchant_data` DISABLE KEYS */;

INSERT INTO `merchant_data` (`id`, `EMAIL`, `COUNTRY`, `BUSINESS_SERVICES`, `BUSINESS_SUB_SERVICES`, `BUSINESS_TYPE`, `BUSINESS_NAME`, `BUSINESS_ABN_ACN`, `FIRST_NAME`, `LAST_NAME`, `PHONE_NUMBER`, `USER_AGENT`, `IP_ADDRESS`, `MERCHANT_ID`, `PERSONAL_STREET`, `PERSONAL_HOUSE_NUMBER`, `PERSONAL_POSTCODE`, `PERSONAL_CITY`, `PERSONAL_STATE`, `LOCATION_TYPE`, `SEASONAL`, `ANNUAL_TURNOVER`, `AVERAGE_TRANSACTION`, `KUNG_FU`)
VALUES
	(1,'hjdfdcjg@eksdjes.com','en-AU',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'j1392400',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'ewfesfsc@efewfe.com','en-AU','health_care_and_fitness','6','company','PyxiApps Australia Pty Ltd','28155648767','ehehheh','erferfureh','0422939929','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36','::1','j3392400','dcdcdjkjbb','343434','3433','brissie','NSW','fixed','no','50000-100000','51-80','Dwunk Ku-a-lo'),
	(3,'brett@mytappr.com','australia','charities_education_and_membership','5','company','TAPPR PTY LTD','158068636','Brett','Hales','0450414130','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36','192.168.0.70','j3392407','Ann Street','Level 2','4006','Fortitude Valley','QLD','fixed','no','0-10000','11-20','Dwunk Ku-a-lo'),
	(4,'ben@mytappr.com','australia','retail','3','company','TAPPR PTY LTD','35158068636','Ben','Lawton','0419171675','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36','192.168.0.19','j4392407','','','','','NSW','moving','yes','0-10000','2150','Dwunk Ku-a-lo'),
	(5,'lauren@mytappr.com','australia','beauty_and_personal_care','0','company','TAPPR PTY LTD','158068636','Lauren','Borger','0421664851','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.122 Safari/537.36','192.168.0.67','j5392407','Brisbane Street','37','4013','brisbane','QLD','fixed','no','500000+','151','Jeet-Kune-Do'),
	(6,'kerry@mytappr.com','australia','professional_services','2','company','PyxiApps Australia Pty Ltd','28155648767','Kerry','Esson','0421245441','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36','192.168.0.54','j6392408','Ann Street','1062','4006','Fortitude Valley','QLD','fixed','no','250000-500000','151','Dwunk Ku-a-lo'),
	(7,'courtney@mytappr.com','australia',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'j7392410',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `merchant_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table merchant_data_card
# ------------------------------------------------------------

CREATE TABLE `merchant_data_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(255) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `BUSINESS_SERVICES` varchar(255) DEFAULT NULL,
  `BUSINESS_SUB_SERVICES` varchar(255) DEFAULT NULL,
  `BUSINESS_TYPE` varchar(255) DEFAULT NULL,
  `BUSINESS_NAME` varchar(255) DEFAULT NULL,
  `BUSINESS_ABN_ACN` varchar(255) DEFAULT NULL,
  `BUSINESS_STREET` varchar(255) DEFAULT NULL,
  `BUSINESS_HOUSE_NUMBER` varchar(255) DEFAULT NULL,
  `BUSINESS_POSTCODE` varchar(255) DEFAULT NULL,
  `BUSINESS_CITY` varchar(255) DEFAULT NULL,
  `BUSINESS_STATE` varchar(255) DEFAULT NULL,
  `TRADING_STREET` varchar(255) DEFAULT NULL,
  `TRADING_HOUSE_NUMBER` varchar(255) DEFAULT NULL,
  `TRADING_POSTCODE` varchar(255) DEFAULT NULL,
  `TRADING_CITY` varchar(255) DEFAULT NULL,
  `TRADING_STATE` varchar(255) DEFAULT NULL,
  `OWNERSHIP_TYPE` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `DOB` varchar(255) DEFAULT NULL,
  `PHONE_NUMBER` varchar(255) DEFAULT NULL,
  `DRIVER_LICENSE` varchar(255) DEFAULT NULL,
  `USER_AGENT` varchar(255) DEFAULT NULL,
  `IP_ADDRESS` varchar(255) DEFAULT NULL,
  `MERCHANT_ID` varchar(255) NOT NULL DEFAULT '',
  `PERSONAL_STREET` varchar(255) DEFAULT NULL,
  `PERSONAL_HOUSE_NUMBER` varchar(255) DEFAULT NULL,
  `PERSONAL_POSTCODE` varchar(255) DEFAULT NULL,
  `PERSONAL_CITY` varchar(255) DEFAULT NULL,
  `PERSONAL_STATE` varchar(255) DEFAULT NULL,
  `LOCATION_TYPE` varchar(255) DEFAULT NULL,
  `SEASONAL` varchar(255) DEFAULT NULL,
  `ANNUAL_TURNOVER` varchar(255) DEFAULT NULL,
  `AVERAGE_TRANSACTION` varchar(255) DEFAULT NULL,
  `KUNG_FU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `MERCHANT_ID` (`MERCHANT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table shipping_details
# ------------------------------------------------------------

CREATE TABLE `shipping_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SHIPPING_STREET` varchar(255) DEFAULT NULL,
  `SHIPPING_HOUSE_NUMBER` varchar(255) DEFAULT NULL,
  `SHIPPING_POSTCODE` varchar(255) DEFAULT NULL,
  `SHIPPING_CITY` varchar(255) DEFAULT NULL,
  `SHIPPING_STATE` varchar(255) DEFAULT NULL,
  `MERCHANT_ID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `MERCHANT_ID` (`MERCHANT_ID`),
  CONSTRAINT `fk_shipping` FOREIGN KEY (`MERCHANT_ID`) REFERENCES `merchant_data_card` (`MERCHANT_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
