        email: sessionStorage.getItem('user_mail'),
        merch_id: sessionStorage.getItem('user_id'),
        business_category: $("#business_category").val(),
        business_sub_category: $("#business_sub_category").val(),
        businessType: $("input[name=businessType]:checked").val(),
        business_abn_acn: $("input#business_abn_acn").val(),
        business_name: $("input#business_name").val(),
        business_number: $("#business_number").val(),
        business_street: $("#business_street").val(),
        business_state: $("#business_state").val(),
        business_city: $("#business_city").val(),
        business_postcode: $("#business_postcode").val(),
        trading_number: $("#trading_number").val(),
        trading_street: $("#trading_street").val(),
        business_state: $("#business_state").val(),
        trading_city: $("#trading_city").val(),
        trading_postcode: $("#trading_postcode").val(),
        ownershipType: $("#ownershipType").val(),
        last_name: $("#personal_last_name").val(),
        first_name: $("#personal_first_name").val(),
        pers_dob: $("input[name=birthdate_day]:checked").val() + "/" + $("input[name=birthdate_month]:checked").val()+ "/" + $("input[name=birthdate_year]:checked").val(),
        phone_number: $("#personal_phone_number").val(),
        driverlicense: $("#driverlicense").val(),
        personal_number: $("#personal_number").val(),
        personal_street: $("#personal_street").val(),
        personal_state: $("#personal_state").val(),
        personal_city: $("#personal_city").val(),
        personal_postcode: $("#personal_postcode").val(),
        averageTransaction: $("#averageTransaction").val(),
        annualTurnover: $("#annualTurnover").val(),
        seasonal: $("input[name=seasonal]:checked").val(),
        locationUse: $("input[name=locationUse]:checked").val(),
        kungfuStyle: $("select[name=kungfuStyle]").val()

//2nd partner selector fro partnership
        partnerinfo

//radio button for optional address
        shared2
