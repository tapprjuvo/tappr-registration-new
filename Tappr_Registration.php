<!DOCTYPE html>
<?php
session_start();
$session_name = "juvoRegister_";

$_SESSION[$session_name."ap_id"] = md5(date("U"));

?>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="lang-en country-AU locale-en-AU js no-touch cssanimations csstransforms csstransforms3d csstransitions video audio svg inlinesvg show-time" lang="en">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta content="yes" name="apple-mobile-web-app-capable">
<title>Registering for Tappr</title>
<meta name="Register for Tappr" content="Register for Tappr and unleash the power of payments and simple analytics for your business">
<meta content="width=device-width, initial-scale=1, user-scalable=no, minimal-ui" name="viewport">
<meta content="authenticity_token" name="csrf-param">
<meta content="KFHT36itPMCR7rboOJt47Gp4OG599idGnbd/YzVIBJ4=" name="csrf-token">
<link rel="icon" href="favicon.ico" type="image/x-icon" />

<!----------  CSS    ----------------->
<link href="css/custom.css" rel="stylesheet" type="text/css">
<link href="css/custom-fields.css" rel="stylesheet" type="text/css">
<link href="css/selector.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/fontfaces.css" rel="stylesheet" type="text/css">
<link href="css/buttons.css" rel="stylesheet" type="text/css">

<!----------  Javascript     ----------------->

<script src="js/jsreport-cf6a718c9c8e4abd53ebf30c8931e071.js" type="text/javascript"></script>
<script src="js/modernizr-ff8ec377fd601edf64c4014ab930ded0.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/canvasloader.js" type="text/javascript"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.confirm.js"></script>

<!----------  Google Analytics     ----------------->
<script src="js/ga.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-35589035-1']);
  _gaq.push(['_trackPageLoadTime']);

  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<style type="text/css">
#containerC {
	position: absolute;
	opacity: 1;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
}
#canvasloader-container {
	position: absolute;
	top: 60%;
	left: 50%;
	z-index: 9999;
}
</style>
</head>
<body>
<!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<div id="canvasloader-container"></div>
<div id="containerC">
	<header class="main-header" data-sq-widget="banner" role="banner" style="background-color: #00ccff;"> 
		<!----------- Image -------------->
		<div class="content pad-horz"> <a href="http://www.mytappr.com"><img src="pics/tappr_logo_white.png" align="center" class="logo"></a> </div>
	</header>
	<div class="content content-pad-vert">
		<noscript id="noscript">
		Javascript is disabled! Please use a browser that's supports it
		</noscript>
		<h2 style="color:#00CCFF">Welcome to Tappr</h2>
		<h4>Get started with Tappr today and run your business</h4>
		<hr class="style-two">
		<div class="form-border row grid-1-2-3">
			<div class="max-col-span-2">
				<div class="row grid-1-3-3">
					<div class="fadeIn" style="padding-left:30px;">
						<h5 style="color:#00ccff; text-align:left;"> Simple sign up</h5>
						<p class="signup-text-non-mobile" style="text-align:left;">Create your Tappr Account today. No credit card details required. Get started in 5 mins.</p>
					</div>
					<div class="fadeIn" style="padding-left:30px;">
						<h5 style="color:#00ccff; text-align:left;"> Pre-Order Tappr Card Reader</h5>
						<p class="signup-text-non-mobile" style="text-align:left;">Pre-Order the Tappr Card Reader now. Only $99 and the first 250 users will receive $99 credit.</p>
					</div>
					<div class="fadeIn" style="padding-left:30px;">
						<h5 style="color:#00ccff; text-align:left;">No commitments required</h5>
						<p class="signup-text-non-mobile" style="text-align:left;">No contracts or commitments. No hidden fees or charges.</p>
					</div>
				</div>
			</div>
		</div>
		<h3>Create a Tappr Account</h3>
		<form accept-charset="UTF-8" class="activation">
			<!--            <form accept-charset="UTF-8" action="#" class="activation" method="post">-->
			<div style="margin:0;padding:0;display:inline">
				<input name="utf8" type="hidden" value="✓">
				<input name="authenticity_token" type="hidden" value="KFHT36itPMCR7rboOJt47Gp4OG599idGnbd/YzVIBJ4=">
			</div>
			<div>
			<div class="row grid-1-2 max-col-span-2">
				<div class="form-border">
					<div class="columnReg"> <img style="max-width:100%; float:left;" src="pics/Tappr_Flagship 2.jpg"> </div>
					<div class="columnReg">
						<div class="row grid-1-5 max-col-span-2" style="margin-bottom: 0px;">
							<div class="field">
								<label for="email">Email address</label>
								<input id="email" name="email" placeholder="your@email.com" type="email">
								<span class="validation-response" id="email"></span> </div>
						</div>
						<div class="row grid-1-5 max-col-span-2"  style="margin-bottom: 0px;">
							<div class="field">
								<div class="input-group-element">
									<label for="password">Create a password</label>
									<input id="password" name="password" placeholder="Password" type="password">
									<span class="validation-response" id="password"></span> </div>
							</div>
						</div>
						<div class="row grid-1-5 max-col-span-2"  style="margin-bottom: 0px;">
							<div class="field">
								<label for="locale_code">Country / Language</label>
								<div class="buttonSel custom-select">
									<select class="country-selector custom-select" data-redirect-params="[&quot;email&quot;,&quot;email_confirmation&quot;,&quot;country&quot;,&quot;country_code&quot;,&quot;locale_code&quot;,&quot;return_to&quot;,&quot;activation_token&quot;,&quot;signup_token&quot;]" id="locale_code" id="country-selector" name="locale_code">
									<option value="australia" selected="selected">Australia</option>
									<option value="singapore" disabled>Singapore</option>
									<option value="newzealand" disabled>New Zealand</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row grid-1-5 max-col-span-2"  style="margin-bottom: 0px;">
							<div class="activation-footer">
								<div class=" grid-1-2-8">
									<div class="fine-print center-vertically-wrap"><span class="center-vertically-text">By creating this account, I agree to Tappr's <a target="_blank" href="http://www.mytappr.com/legal/sellers-agreement">Seller Agreement</a> and <a target="_blank" href="http://www.mytappr.com/legal/e-sign-consent-agreement">E-Sign Consent</a>.</span></div>
									<field class="js-field-view continue continue-button-field signup-button-field">
										<input class="button button-blue" disabled data-disable-with="Submitting..." id="commit" name="commit" type="button" onclick="create_account();" value="Create Account">
									</field>
									<!--<field class="max-col-span-2">
										<div class="fine-print center-vertically-wrap"><span class="center-vertically-text"><a href="#">I already have a Tappr account</a></span></div>
									</field>--> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<footer id="tappr_footer">
    <hr class="style-two">
	<div class="copyright fine-print" id="simple_footer">© 2014 Tappr, Inc. All rights reserved. </div>
</footer>
</div>
<!--    Page scripts--> 
<script src="js/custom.js" type="text/javascript"></script> 
<script src="js/verifyaccount.js.php" type="text/javascript"></script>
</body>
</html>