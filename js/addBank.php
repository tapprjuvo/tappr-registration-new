<?php
session_start();
$session_name = "juvoRegister_";

header('Content-type: application/json');

$return['error'] = true;

function clearData($val, $quotes = true) {
    $val = addslashes(trim($val));
    $val = str_replace("&", "", $val);
    
    return $quotes ? "'$val'" : $val;
}

    $conn = mysqli_connect("192.168.0.33", "root", "sysdba", "tappr_reg");
    
    // Check connection
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
    if ($conn) {
        $sql = "INSERT INTO bank_account_details(" . 
            "BANK_ACCOUNT_TYPE, BANK_ACCOUNT_NAME, BANK_ACCOUNT_NR, BANK_ACCOUNT_BSB, MERCHANT_ID" . ") VALUES (" .
            clearData($_REQUEST["email"]). ", " .
            clearData($_REQUEST["email"]). ", " .
            clearData($_REQUEST["email"]). ", " .
            clearData($_REQUEST["email"]). ", " .
            clearData($_REQUEST["merch_id"]) . ")";
        
        $insert = mysqli_query($conn, $sql);
        
        if ($insert) {
            $return['error'] = false;
        } else {
            echo "Error: " . mysqli_error($conn);
        }
        mysqli_close($conn);
    } else {
        echo "Could not connect to the DB: " . mysqli_error($conn); //redundant?
        exit;
    }

echo json_encode($return);

?>