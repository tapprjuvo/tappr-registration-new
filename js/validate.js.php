<?php
session_start();
$session_name = "juvoRegister_";
?>
/*************************************

	is_valid

*************************************/

function is_valid(fld, tests){

	var val = $.trim($(fld).val());
	var field_type = arguments[1] ? arguments[1] : "text";
	var num_test = /^(?=.*\d.*\d).*$/;
	var symbol = /^(?=.*[!@#$%^&*]).*$/;
	//console.log(fld);
	var correct = true;
	var i, response = "", count = 0;
	//console.clear();

	for(i=0; i<tests.length; i++){

		count++;
		//console.log(typeof(tests[i]));

		if (typeof(tests[i]) == "number") {

			correct = val.length >= tests[i];

			if (!correct) {

				response = "Must be at least 8 characters long";

			}

		}else{

			switch(tests[i]){

			case "human":

				correct = false;
				if(val==<?php echo $_SESSION[$session_name."human"]; ?>){
						correct = true;
				}

				if (!correct) {

					response = "Are you sure you're not a robot?";

				}

			break;

				case "empty":
					correct = val.length > 0;

					if (!correct) {

						response = "Value required";

					}
					break;

				case "abn":
					var test = $(fld).attr("data-test");
					var testCheck = $(fld).attr("data-test-check");
					var valid = $(fld).attr("data-valid");

					//console.log(test + ", " + testCheck + ", " + valid);

					if(testCheck != valid && $(fld).val() != ""){

						if(testCheck == ""  && valid == ""){

							correct = false;
							response = "Invalid ABN/ACN";

						}else{

							if(testCheck != valid){

								/*
								correct = $(this).val()  == test;

								if (!correct) {

									response = "Invalid ABN/ACN ya hear!";

								}else{


								}
								*/
									//console.log("hmmm");

								correct = false;
								response = test != "" ? "Validating" : "Invalid ABN/ACN";

							}else{

								correct = false;
								response = test != "" ? "Validating" : "Invalid ABN/ACN";

								//console.log("aha");

							}

						}
					}else{

						/*
						correct = $(this).val()  == valid;

						if (!correct) {

							response = "Invalid ABN/ACN";

						}
						*/

						correct = $(fld).attr("data-good") == "yes";

						if(!correct){
							response = $(fld).attr("data-response") != "" ? $(fld).attr("data-response") : "Invalid ABN/ACN";
						}
						//console.log("oh boy");
						//console.log(correct ? "ya ya" : "neigh");

					}
					break;

				case "email":
					correct = checkEmail(val);

					if (!correct.correct) {

						response = correct.response;

					}

					correct = correct.correct;
					break;

				case "numbers":
				   correct = num_test.test(val);

					if (!correct) {

						response = "Must contain at least 2 numbers";

					}
					break;


			}
		}

		if (!correct) {

			break;

		}
	}

	return {
		 correct: correct,
		 response : response
	};

}

/*************************************

	show_response

*************************************/


function show_response(tgt, type, valid, check, init){

	//console.log(tgt + " - " + ((!init || $.trim($("#" + tgt + " " + type).val()) != "") ? "show" : "hide"));

	if ((!check || check == tgt) && (!init || $.trim($("#" + tgt + " " + type).val()) != "")) {


		if (!valid.correct) {

			$("#" + tgt + " .validation-response").text(valid.response).fadeIn();

		}else{

			$("#" + tgt + " .validation-response").fadeOut(function(){$(this).empty()});

		}
	}

}

/*************************************

	password input

*************************************/
$("#step1_next").on("click",function(){
	$('body').css('overflow','auto');
});

$("#password input").on("change",function(){

	step1_proceed("password");

});

/*************************************

	step 1 fields

*************************************/

$("#password input").on("blur", function(e){

	//console.log("a");

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			step1_proceed($(this).attr("name"));
			break;
	}


});

$("#email input").on("blur", function(e){
	var len = $(this).val();
	$.post("/validation/validate.php",{"field[EMAIL]":$(this).val()},function(data){

			if(data=='false'){
					if(len==''){
						$(".validation-response","#email").text('Value required').fadeIn();
					}else{
				$(".validation-response","#email").text("The email doesn't seem to be valid").fadeIn();
			}
			}else if(data=='taken'){
				$(".validation-response","#email").text("The email is already registered with Tappr").fadeIn();
			}else{
				$(".validation-response","#email").text('').fadeIn();
			}
	});

if($(this).val()==''){
	$(".validation-response","#email").text('Value required').fadeIn();
}

});


$("#email input, #password input").on("change",function(){

	step1_proceed($(this).attr("name"));

});

$("#country select").on("change",function(){

	step1_proceed($(this).attr("name"));

});

/*************************************

	checkEmail

*************************************/

function checkEmail(emailStr){

	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s"+specialChars+"\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars+'+';
	var word="("+atom+"|"+quotedUser+")";
	var userPat=new RegExp("^"+word+"(\\."+word+")*$");
	var domainPat=new RegExp("^"+atom+"(\\."+atom+")*$");
	var matchArray=emailStr.match(emailPat);
	var error_message;

	if(matchArray==null){

		return {correct: false, response: "Check @ and .'s"};

	}

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null){

		return {correct: false, response: "The email doesn't seem to be valid"};

	}

	var IPArray=domain.match(ipDomainPat);

	if(IPArray!=null){

		for(var i=1;i<=4;i++){

			if(IPArray[i]>255){

				return {correct: false, response: "Destination IP address is invalid"};

			}
		}

		return {correct:true};
	}

	var domainArray=domain.match(domainPat);

	if(domainArray==null){

		return {correct: false, response: "Domain name doesn't seem to be valid"};

	}

	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2||domArr[domArr.length-1].length>3){

		return {correct: false, response: "Must end with 3 letter domain or 2 letter country"};

	}

	if(len<2){



	}

	return {correct:true};

}

/*************************************

	step1_proceed

*************************************/

var valid = ['top'];

function step1_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];

	//console.log(!check ? "all" : check);

	valid = is_valid($("#email input"), ["empty", "email"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("email", "input", valid, check, init);

	valid = is_valid($("#password input"), ["empty", "numbers", 8]);


	if (!valid.correct) {

		correct = false;

	}

	show_response("password", "input", valid, check, init);

	valid = {correct: $( "#country option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please choose a country";

	}

	show_response("country", "option:selected", valid, check, init);

	if (correct) {

		$("#step1_next, #what-you-do select").removeAttr("disabled");
		$("#what-you-do").css("opacity", 1);
		window.valid.push($("section#top").next("section").attr("id"));

		step2_proceed(false, true);

	}else{

		$("#step1_next, #what-you-do select").attr("disabled","disabled");
		$("#what-you-do").css("opacity", .2);
		removeA(window.valid, $("section#top").next("section").attr("id"));

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) === false) {

			if(init){

				runScroller = true;

				$('html,body').animate({ scrollTop: $("section#top").offset().top},'fast',function(){

					runScroller = false;
					currentSection = $("section:in-viewport");

				});
			}
		}

		step2_proceed(false, true, true);

	}

	//if(!inBox()){

		$("#what-you-do select").selectpicker("refresh");

	//}

	return correct;

}

/*************************************

	step 2 fields

*************************************/

//var retailVal = "";

$("#services select").on("change",function(){

	//step2_proceed($(this).attr("name"));

	var val = $(this).val(), i;
	//var oldVal = retailVal;

	//retailVal = $("#retail_type select").val();

	if(!inBox()){

		//$("#what-you-do select").selectpicker("destroy");

	}

	$("#retail_type select").empty();

	//console.log(val);

	if(val == ""){

		$("#retail_type select").append("<option value=''>Please choose a service first</option>");

	}else{

		$("#retail_type select").append("<option value=''>Please select</option>");

	}

	$.each(serviceList, function(key, value){

		if(key == val){

			//console.log("key = " + key + "\nval = " + val);

			$.each(value, function(index, nVal){

				$("#retail_type select").append("<option>" + nVal + "</option>");

			});

		}

	});

	//$("#retail_type select").val(oldVal);

	//if(!inBox()){

		$("#what-you-do select").selectpicker("refresh");

	//}

});

$("#what-you-do select").on("change",function(){

	step2_proceed($(this).attr("name"));

});

/*************************************

	step2_proceed

*************************************/

function step2_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];

	//console.log("val = " + $("#services option:selected" ).val());


	valid = {correct: $( "#services option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("services", "option:selected", valid, check, init);

	valid = {correct: $( "#retail_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("retail_type", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

	if (correct) {

		$("#step2_next, #businessUse input[type=text], #businessUse select").removeAttr("disabled");
		$("#businessUse").css("opacity", 1);
		window.valid.push($("section#what-you-do").next("section").attr("id"));
		step3_proceed(false, true);

	}else{

		$("#step2_next, #businessUse input[type=text], #businessUse select").attr("disabled","disabled");
		$("#businessUse").css("opacity", .2);
		removeA(window.valid, $("section#what-you-do").next("section").attr("id"));
		step3_proceed(false, true, true);

	}

	//if(!inBox()){

		$("#businessUse select").selectpicker("refresh");

	//}

	return correct;

}

/*************************************

	step 3 fields

*************************************/

$("#businessUse input[type=text]").on("blur", function(e){

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			step3_proceed($(this).attr("name"));
			break;
	}


});

$("#business_abn_acn input[type=text]").on("blur", function(e){

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			console.clear();
			var weighting, i, len, check, acnVal = 8, lastDig, num, valid = {}, correct = false; tot = 0, abn = false, acn = false, val = $.trim($(this).val());
			var reg = new RegExp(/\d+/g);

			//console.log("val start = " + val);

			val = String(val.replace(/[^0-9]+/g, ''));

			//console.log("val numbers = " + val);

			if(val == null){

				//console.log("it's nothing");
				return;

			}else{

				len = val.length;

				//console.log("length = " + len);

				if(len == 9){

					// ACN

					//http://www.asic.gov.au/asic/asic.nsf/byheadline/Australian+Company+Number+%28ACN%29+Check+Digit#

					//console.log("acn");

					//weighting = [8,7,6,5,4,3,2,1];

					for(i=0; i<8; i++){

						check = Number(val.charAt(i));

						tot += check * acnVal;

						//console.log(check + " x " + acnVal + " = " + (check * acnVal));

						--acnVal;

					}

					//console.log("tot = " + tot );
					//console.log("tot % 10 = " + (tot % 10));

					tot = tot % 10;

					//console.log("10 - tot = " + (10 - tot));

					tot = 10 - tot;

					lastDig = Number(val.charAt(8));

					//console.log("lastDig = " + lastDig);

					//console.log(tot == lastDig ? "acn is fine" : "acn is bad");

					correct = lastDig == tot;

				}else if(len == 11){

					correct = true;

					/*

					It looks like this no longer applies as real abn dont always work

					https://www.ato.gov.au/Business/Australian-business-number/In-detail/Introduction/Format-of-the-ABN/

					// ABN

					console.log("abn");

					weighting = [10,1,3,5,7,9,11,13,15,17,19];

					for(i=0; i<val.length; i++){

						check = Number(val.charAt(i));

						if(i == 0){

							--check;

						}

						tot += check * weighting[i];

						console.log(check + " x " + weighting[i] + " = " + (check * weighting[i]));

					}

					console.log("tot = " + tot );
					console.log("tot / 89 = " + (tot / 89));

					tot = tot / 89;

					console.log("tot%5 = " + tot%5);

					console.log(tot%5 == 0 ? "abn is fine" : "abn is bad");

					correct = tot % 5 == 0;
					*/

				}else{

					//console.log("number count wrong");

				}
			}

			valid.correct = correct;

			if(valid.correct){

				//$(this).attr("data-test", $(this).val());
				//console.log("gut");

			}else{

				$(this).attr("data-test", "");
				$(this).attr("data-test-check", "");
				$(this).attr("data-valid", "");
				$(this).attr("data-good", "no");
				$(this).attr("data-response", "Invalid ABN/ACN");
				valid.response = "Invalid ABN/ACN";
				//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
				step3_proceed($(this).attr("name"));
				//console.log("nicht gut")
				return;

			}

			//if($(this).attr("data-test") != $(this).attr("data-valid") && !$(this).hasClass("validadting")){
			if(val != $(this).attr("data-valid") && $(this).val() != $(this).attr("data-test")){

				//$(this).addClass("validadting");
				$(this).attr("data-test", $(this).val());
				$(this).attr("data-test-check", val);

				//return;

				if(val != ""){

					var tgt = this;
					var testVal = $(this).val();
					step3_proceed($(tgt).attr("name"));

					$.ajax({
						type: "post",
						data: {
							abn: val
						},
						dataType: "json",
						url: "abn.php"
					}).done(function(data){

						//console.log(data);
						//console.log(data.error);

						//var html;
						//$(this).removeClass("validadting");

						if($(tgt).val() == testVal){

							$(tgt).attr("data-valid", "");

							if(data.success){

								correct = true;
								//console.log("ya = " + val + ".");



								if(confirm("Is this you?\n\n" + data.entityName)){

									$(tgt).attr("data-valid", val);
									//$(tgt).val(val);

									step3_proceed($(tgt).attr("name"));
									$(tgt).attr("data-good", "yes");

									$("#business_name input").val(data.entityName);
									$("#business_abn_acn input").val(data.abnacn);

								}else{

									$(tgt).attr("data-valid", "");
									$(tgt).attr("data-test", "");
									$(tgt).attr("data-test-check", "");
									$(tgt).attr("data-good", "no");
									$(tgt).attr("data-response", "");
									//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
									step3_proceed($(tgt).attr("name"));

								}

								step3_proceed($(tgt).attr("name"));

							}else{

								correct = false;
								response = data.reason;
								//console.log("na");

								valid.correct = false;
								valid.response = data.reason;
								$(tgt).attr("data-valid", "");
								$(tgt).attr("data-test", "");
								$(tgt).attr("data-test-check", "");
								$(tgt).attr("data-good", "no");
								$(tgt).attr("data-response", data.reason);
								//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
								step3_proceed($(tgt).attr("name"));

							}

						}

					}).fail(function(jqXHR, textStatus){

						if($(tgt).val() == testVal){

							correct = false;
							response = "could not connect";
							$(tgt).attr("data-valid", "");
							step3_proceed($(tgt).attr("name"));

						}

					});
				}

			}else{

				//console.log("hooey");

			}
			break;
	}


});

$("#businessUse input[type=text]").on("change",function(){

	step3_proceed($(this).attr("name"));

});

$("#businessUse select").on("change",function(){

	step3_proceed($(this).attr("name"));

});


/*************************************

	step3_proceed

*************************************/

function step3_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];

	valid = is_valid($("#business_name input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("business_name", "input", valid, check, init);

	valid = is_valid($("#business_abn_acn input"), ["empty", "abn"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("business_abn_acn", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("bus_type", "option:selected", valid, check, init);

	valid = {correct: $( "#annual_turnover option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("annual_turnover", "option:selected", valid, check, init);

	valid = {correct: $( "#employees option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("employees", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

    show_response("bus-address", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

    show_response("city", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

    show_response("post-code", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

    show_response("state", "option:selected", valid, check, init);

	valid = {correct: $( "#seasonal-bus option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("seasonal-bus", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

	if (correct) {

		$("#step3_next, #kungfu select").removeAttr("disabled");
		window.valid.push($("section#businessUse").next("section").attr("id"));
		$("#kungfu").css("opacity", 1);
		step4_proceed(false, true);

	}else{

		$("#step3_next, #kungfu select").attr("disabled","disabled");
		removeA(window.valid, $("section#businessUse").next("section").attr("id"));
		$("#kungfu").css("opacity", .2);
		step4_proceed(false, true, true);

	}

	//if(!inBox()){

		$("#kungfu select").selectpicker("refresh");

	//}

	return correct;

}

/*************************************

	step 4 fields

*************************************/

$("#kungfu select").on("change",function(){

	step4_proceed($(this).attr("name"));

});

/*************************************

	step4_proceed

*************************************/

function step4_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];

	valid = {correct: $( "#kung_fu option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("kung_fu", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

	if (correct) {

		$("#step4_next, #about_you input[type=text]").removeAttr("disabled");
		window.valid.push($("section#kungfu").next("section").attr("id"));
		$("#about_you").css("opacity", 1);
		step5_proceed(false, true);

	}else{

		$("#step4_next, #about_you input[type=text]").attr("disabled","disabled");
		removeA(window.valid, $("section#kungfu").next("section").attr("id"));
		$("#about_you").css("opacity", .2);
		step5_proceed(false, true, true);

	}


	//$("#about_you select").selectpicker("refresh");
	return correct;

}

/*************************************

	step 5 fields

*************************************/

$("#about_you input[type=text]").on("blur", function(e){

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			step5_proceed($(this).attr("name"));
			break;
	}


});

/*************************************

	step5_proceed

*************************************/

function step5_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];


	valid = is_valid($("#first_name input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("first_name", "input", valid, check, init);

	valid = is_valid($("#last_name input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("last_name", "input", valid, check, init);

	valid = is_valid($("#mobile input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("mobile", "input", valid, check, init);


		valid = is_valid($("#human input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}



		valid = is_valid($("#human input"), ["human"]);

		if (!valid.correct) {
		console.log(valid.response);
			correct = false;

		}

	show_response("human", "input", valid, check, init);



	if(setFalse){

		correct = false;

	}


	if (correct) {

		if($("#hdjksf-dsuf-ds8-90ad-sfd").val() == "789"){

			$("#step5_next, #humanity-check").removeAttr("disabled");

			$("#step5_next, #got-to-create").removeAttr("disabled");

		}else{

			$("#step5_next, #got-to-create").removeAttr("disabled");

		}

		window.valid.push($("section#about_you").next("section").attr("id"));
		$("#send_data").css("opacity", 1);

	}else{

		if($("#hdjksf-dsuf-ds8-90ad-sfd").val() == "789"){

			$("#step5_next, #humanity-check").attr("disabled","disabled");
			$("#step5_next, #got-to-create").attr("disabled","disabled");
		}else{

			$("#step5_next, #got-to-create").attr("disabled","disabled");

		}

		removeA(window.valid, $("section#about_you").next("section").attr("id"));
		$("#send_data").css("opacity", .2);

	}


	//$("#about_you select").selectpicker("refresh");
	return correct;

}

/*************************************

	scrollPage

*************************************/

function scrollPage(){

	var tgt = arguments[0] ? arguments [0] : $("section:in-viewport").next();

	runScroller = true;

	$('html,body').animate({ scrollTop: tgt.offset().top}, 500,function(){
		runScroller = false;
		currentSection = $("section:in-viewport");
	});

}

/*************************************

	stopWheel

*************************************/

var runScroller = false;
var currentSection = $("#top");
//var gesture = true;

function stopWheel(e){

	if(inBox()){

		return true;

	}

	//console.log("w = " + winHeight + ", m = " + minHeight);

	var prevframe, thisframe, nextframe, wheel;

	e = !e ? window.event : e;

	e.preventDefault();
	e.returnValue = false;
	e.stopPropagation();

	if(runScroller==true){

		return false;

	}

	if (e.type == 'mousewheel') {

		wheel = e.originalEvent.wheelDelta;

	}else if (e.type == 'DOMMouseScroll') {

		wheel = e.originalEvent.detail;

	}

	thisframe = $("section:in-viewport").attr("id");
	nextframe = $("section:in-viewport").next().attr("id");
	prevframe = $("section:in-viewport").prev().attr("id");

	//console.clear();
	//console.log("wheel = " + wheel + "\nthisframe = " + thisframe + "\nnextframe = " + nextframe + "\nprevframe = " + prevframe);
	//console.log(valid);

	if( wheel > 0 ){

		if(typeof nextframe != "undefined"  && $.inArray(nextframe,valid) > -1){

			scrollPage($("#"+nextframe));
			return false;

		}
	}

	if( wheel < 0 ){

		if( typeof prevframe!="undefined" && $.inArray(prevframe,valid) > -1 ){

			//console.log("here\n" + $("#"+prevframe).offset().top);

			scrollPage($("#"+prevframe));
			return false;

		}
	}
}

/*************************************

	stopSwipe

*************************************/

var touchY;

/*
$(function() {
      //Enable swiping...
      $(document).swipe( {
        //Generic swipe handler for all directions
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
          //alert("You swiped " + direction );
        },
        //Default is 75px, set to 0 for demo so any distance triggers swipe
         //threshold:0
      });
    });

$(document).bind('touchstart', function(e) {

	touchY = e.originalEvent.pageY;
	//e.stopImmediatePropagation();
	//e.preventDefault();

});

$(document).bind('touchmove', function(e) {

	e.stopImmediatePropagation();
	e.preventDefault();

});

$(document).bind('touchend', function(e) {
	stopSwipe(e)
	//alert("swiper no swiping");
});
*/

function stopSwipe(direction){

	alert(direction);
	return;

	var touchEnd = e.originalEvent.pageY;

	if(touchY == touchEnd){

		return;

	}

	if(runScroller==true){

		return false;

	}

	var direction = touchY < touchEnd ? "down" : "up";

	//alert("swiper");

	thisframe = $("section:in-viewport").attr("id");
	nextframe = $("section:in-viewport").next().attr("id");
	prevframe = $("section:in-viewport").prev().attr("id");

	if( direction == "down" ){

		alert("hi " + thisframe + "\nnextframe = " + nextframe);

		if(typeof nextframe != "undefined"  && $.inArray(nextframe,valid) > -1){

			scrollPage($("#"+nextframe));
			return false;

		}
	}

	if( direction == "up" ){

		if( typeof prevframe!="undefined" && $.inArray(prevframe,valid) > -1 ){

			//console.log("here\n" + $("#"+prevframe).offset().top);

			scrollPage($("#"+prevframe));
			return false;

		}
	}

	return false;


}

/*************************************

	try_again();

*************************************/

function try_again() {

	$("#got-to-create").show();
	$("#loader").hide();
	$("#send-data-start").show();
	$("#send-data-bad").hide();
	$("#send-data-good").hide();

}




/*************************************

	create_account();

*************************************/

function create_account() {

//	if($("#hdjksf-dsuf-ds8-90ad-sfd").val() != "789"){

		$("#got-to-create").hide();
		
		if(isiOS) {
			window.location = "juvo://clickedRegisterButton";
		} else {
			$("#loader").show();
		}

		var data={
			register: "yes",
			dCheck: $("#hdjksf-dsuf-ds8-90ad-sfd").val(),

			email: $("#email input").val(),
			password: $("#password input").val(),
			country: $("#country select").val(),

			services: $("#services select").val(),
			retail_type: $("#retail_type select").val(),

			business_name: $("#business_name input").val(),
			business_abn_acn: $("#business_abn_acn input").attr("data-valid"),
			bus_type: $("#bus_type select").val(),
			annual_turnover: $("#annual_turnover select").val(),
			employees: $("#employees select").val(),

			kung_fu: $("#kung_fu select").val(),

			first_name: $("#first_name input").val(),
			last_name: $("#last_name input").val(),
			dob: $("#dob_val").val(),
			mobile: $("#mobile input").val(),

			win_height: $(window).height(),
			win_width: $(window).width()

		}

		$.ajax({
			type: "post",
			data: data,
			dataType: "json",
			url: "connect.php"
		}).done(function(data){

			//console.log(data);
			//console.log(data.error);

			//var html;

			if(data.success){

				onRegistrationSuccessForiOS(data);

				$("#got-to-juvo, #pre-order-juvo").removeAttr("disabled");
				$("#send-data-start").hide();
				$("#send-data-bad").hide();
				$("#send-data-good").show();

				//html = "good - " + data.result;
				$("#juvo-link").val(data.result);

			}else{

				onRegistrationFailureForiOS();
				
				$("#try-again").removeAttr("disabled");
				$("#got-to-juvo, #pre-order-juvo").attr("disabled","disabled");
				$("#fail-reason").html(data.error);
				//html = "bad: " + data.error;
				$("#send-data-start").hide();
				$("#send-data-good").hide();
				$("#send-data-bad").show();

			}

			//$("#test-data").html(html);

		}).fail(function(jqXHR, textStatus){

			//$("#test-data").html("Unable to save settings: " + textStatus + "<br>" + data);
			
			onRegistrationFailureForiOS();

			$("#try-again").removeAttr("disabled");
			$("#got-to-juvo, #pre-order-juvo").attr("disabled","disabled");
			$("#fail-reason").html(textStatus);
			//$("#send-data-bad .start-text").html("Unable to save settings: " + textStatus);
			$("#send-data-start").hide();
			$("#send-data-good").hide();
			$("#send-data-bad").show();

		});
//	}

}

/*************************************

	removeA

*************************************/

function removeA(arr) {

	var what, a = arguments, L = a.length, ax;

	while (L > 1 && arr.length) {

		what = a[--L];

		while ((ax= arr.indexOf(what)) !== -1) {

			arr.splice(ax, 1);

		}
	}

	return arr;

}

/*************************************

	winTall

*************************************/

function winTall(){

	var tall = $(window).height() < minScreenTall.height;
	var wide = $(window).width() < minScreenTall.width;

	return $(window).height() < minScreenTall.height && $(window).width() < minScreenTall.width;


}

/*************************************

	winWide

*************************************/

function winWide(){

	var tall = $(window).height() < minScreenWide.height;
	var wide = $(window).width() < minScreenWide.width;

	return $(window).height() < minScreenWide.height && $(window).width() < minScreenWide.width;


}

/*************************************

	inBox

*************************************/

function inBox(){

	if(winWide()){

		return true;

	}

	if(winTall()){

		return true;

	}

	return false;


}

winHeight = $(window).height();
winWidth = $(window).width();
var minHeight = 400;
var minScreenTall = {height: 650, width: 470};
var minScreenWide = {height: 470, width: 650};

/*************************************

	ready

*************************************/

/*
w 360
h 567
*/

//$('.selectpicker').selectpicker();

$().ready(function(){

	if(!inBox()){

		$('.parallax').scrolly({bgParallax: true});

	}

	//console.log(winHeight + ", " + winWidth);

	step1_proceed(false, true);

	var d = new Date();

	var yearNow = d.getFullYear() - 18;
	var yearStart = yearNow - 80;

	$( "#dob input" ).datepicker({
		dateFormat: "d MM yy",
		defaultDateType: 0,
		yearRange: yearStart+ ":" + yearNow,
		changeMonth: true,
		changeYear: true,
		altField: "#dob_val",
		altFormat: "yy-mm-dd",
		maxDate: "-18y"
	});

	//$( "#dob input" ).datepicker("setDate"

	$(document).on("mousewheel DOMMouseScroll", function(event){

	//	stopWheel(event);

	});

	$("section").blur(function(){

		var home = $("*:focus").closest("section");

		//console.log("i am in " + home.attr("id"));
		//console.log($("*:focus").closest("section").offset());

		if(home.attr("id") !== undefined){

			var tgtId = $("*:focus").closest("section").attr("id");
			var curId = currentSection.attr("id");

			if(tgtId != curId){

				scrollPage($("*:focus").closest("section"));

			}
		}
	});

	$(document).on("keypup", "section", function(e){

	});

	$("body").delegate(".dropdown-menu, .ui-datepicker-title select", "mousewheel DOMMouseScroll", function(e){

		e = !e ? window.event : e;

		//e.preventDefault();
		//e.returnValue = false;
		e.stopPropagation();

	});

	$(".next-btn").on("click",function(){

		if (!$(this).hasClass("inactive")) {

			var next_section = $(this).closest("section").next("section");
			scrollPage(next_section);

		}
	});

    $(".next-btn-bus").on("click",function(){

		if (!$(this).hasClass("inactive")) {

			var next_section = $(this).closest("section").next("section");
			scrollPage(next_section);

		}
	});

	$(window).resize(function(){


		winHeight = $(window).height();
		winWidth = $(window).width();

		if(!inBox()){

			$('html,body').scrollTop(currentSection.offset().top);

		}
		return;


	});

	//alert("width = " + $(window).width());
	//alert("height = " + $(window).height());

});


var serviceList = {
	"Beauty & Personal Care": [
		"Beauty Salon",
		"Hair Salon/ Barbershop",
		"Independent Stylist/ Barber",
		"Massage Therapist",
		"Nail Salon",
		"Spa",
		"Tanning Salon",
		"Tattoo/ Piercing",
		"Other"
	],
	"Charity": [
		"Charitable Organisation",
		"Child Care",
		"Instructor/ Teacher",
		"Membership Organisation",
		"Political Organisation",
		"Religious Organisation",
		"School",
		"Tutor",
		"Other"
	],
	"Food & Drink": [
		"Bakery",
		"Bar/ Club/ Lounge",
		"Caterer",
		"Coffee/ Tea Store",
		"Convenience Store",
		"Food Truck/ Cart",
		"Grocery/ Market",
		"Outdoor Market",
		"Private Chef",
		"Quick Service Restaurant",
		"Sit Down Restaurant",
		"Speciality Store",
		"Other"
	],
	"Health Care": [
		"Acupuncture",
		"Alternative Medicines",
		"Carer",
		"Chiropractor",
		"Dentist/ Orthodontist",
		"Gym",
		"Massage Therapist",
		"Medical Practitioner",
		"Optometrist/ Eye Care",
		"Personal Trainer",
		"Psychiatrist",
		"Therapist",
		"Veterinary Services",
		"Other"
	],
	"Home & Repair": [
		"Automotive Parts, Repair",
		"Carpentry Services",
		"Carpet Cleaning",
		"Cleaning",
		"Clothing Repair/ Shoe Repair",
		"Computer/ Electronics Repair",
		"Dry Cleaning",
		"Electrical Services",
		"Flooring",
		"General Contracting",
		"Heating & Air-Con",
		"Installation Services",
		"Junk Removal",
		"Landscaping",
		"Locksmiths Services",
		"Masonry, Stonework",
		"Moving/ Removalist",
		"Painting",
		"Pest Control",
		"Plumbing",
		"Roofing",
		"Watch/ Jewellery Services",
		"Other"
	],
	"Leisure":[
		"Events/ Festivals",
		"Movies/ Film",
		"Museum/ Cultural",
		"Music",
		"Performing Arts",
		"Sporting Events",
		"Sport Recreation",
		"Tourism",
		"Other"
	],
	"Professional Services": [
		"Accounting",
		"Child Care",
		"Consulting",
		"Delivery",
		"Design",
		"Interior Design",
		"Legal Services",
		"Marketing/ Advertising",
		"Nanny Services",
		"Photography",
		"Printing",
		"Real Estate",
		"Software Development",
		"Other"
	],
	"Retail":[
		"Art, Photo, Film",
		"Books, Magazines, Music, Video",
		"Clothing and Accessories",
		"Convenience Store",
		"Electronics Store",
		"Eye Wear Store",
		"Flowers & Gifts",
		"Furniture",
		"Grocery/ Markets",
		"Hardware Store",
		"Hobby Shop",
		"Jewellery Store",
		"Office Supply",
		"Outdoor Markets",
		"Pet Store",
		"Publishing/ Printing",
		"Speciality Store",
		"Sporting Goods",
		"Other"
	],
	"Transportation":[
		"Bus Services",
		"Delivery",
		"Limousine",
		"Moving",
		"Private Shuttle",
		"Taxi Services",
		"Town Car Services",
		"Other"
	],
	"Causal Use":[
		"Events",
		"Miscellaneous Goods",
		"Miscellaneous Services",
		"Outdoor Markets/ Stands",
		"Other"
	]
};

// possible native mobile select function

/*
*/
