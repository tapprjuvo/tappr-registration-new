/**
 Called from the iOS app to get the response from the registration ajax call
*/
var registrationResponse = "";
function getRegistrationResponse() {
	return registrationResponse;
}

/**
 When called runs various window.location events to allow the iOS app to update
 Called by the iOS app
*/
var isiOS = false;
function iOS() {
	isiOS = true;
}

/**
 To be called when the registration ajax call is successful 
*/
function onRegistrationSuccessForiOS(data) {
	if(!isiOS) {
		return;
	}
	registrationResponse = JSON.stringify(data);
	window.location = "juvo://onRegistrationAJAXDone/";
}

/**
 To be called when the registration ajax times out or fails for any other reason
*/
function onRegistrationFailureForiOS() {
	if(!isiOS) {
		return;
	}
	window.location = "juvo://onRegistrationFailure/" + textStatus;
}
