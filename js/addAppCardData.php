<?php
error_reporting(E_ALL);
/*session_start();
$session_name = "juvoRegister_";*/

header('Content-type: application/json');

//$return['error'] = true;
$return = array(
    "success" => false,
    "errorLog" => "",
    "query" => ""
);

function clearData($val, $quotes = true) {
    $val = addslashes(trim($val));
    $val = str_replace("&", "", $val);
    
    return $quotes ? "'$val'" : $val;
}

$conn = mysqli_connect("192.168.0.33", "root", "sysdba", "tappr_reg");
    
    // Check connection
if (mysqli_connect_errno()) {
    $return['errorLog'] = "Failed to connect to MySQL: " . mysqli_connect_error();
} 
    
if ($conn) {
    
    $owntype=$_REQUEST["ownershipType"];
    //$owntype="company";
        $sql = "UPDATE merchant_data_card SET " . 
          "business_services=" . clearData($_REQUEST["business_category"]) . ", " .
            "business_sub_services=" . clearData($_REQUEST["business_sub_category"]) . ", " .
            "business_type=" . clearData($_REQUEST["businessType"]) . ", " .
            "business_name=" . clearData($_REQUEST["business_name"]) . ", " .
            "business_abn_acn=" . clearData($_REQUEST["business_abn_acn"]) . ", " .
			"BUSINESS_STREET=" . clearData($_REQUEST["business_street"]) . ", " .
			"BUSINESS_HOUSE_NUMBER=" . clearData($_REQUEST["business_number"]) . ", " .
			"BUSINESS_POSTCODE=" . clearData($_REQUEST["business_postcode"]) . ", " .
			"BUSINESS_CITY=" . clearData($_REQUEST["business_city"]) . ", " .
			"BUSINESS_STATE=" . clearData($_REQUEST["business_state"]) . ", " .
			"TRADING_STREET=" . clearData($_REQUEST["trading_street"]) . ", " .
			"TRADING_HOUSE_NUMBER=" . clearData($_REQUEST["trading_number"]) . ", " .
			"TRADING_POSTCODE=" . clearData($_REQUEST["trading_postcode"]) . ", " .
			"TRADING_CITY=" . clearData($_REQUEST["trading_city"]) . ", " .
			"TRADING_STATE=" . clearData($_REQUEST["trading_state"]) . ", " .
			"OWNERSHIP_TYPE=" . clearData($_REQUEST["ownershipType"]) . ", " .
            "owner_nr=" . clearData($_REQUEST["ownerNr"]) . ", " .
            "first_name=" . clearData($_REQUEST["first_name"]) . ", " .
            "last_name=" . clearData($_REQUEST["last_name"]) . ", " .
			"DOB=" . clearData($_REQUEST["pers_dob"]) . ", " .
            "phone_number=" . clearData($_REQUEST["phone_number"]) . ", " .
			"DRIVER_LICENSE=" . clearData($_REQUEST["driverlicense"]) . ", " .
            "user_agent=" . clearData($_SERVER["HTTP_USER_AGENT"]) . ", " .
            "ip_address=" . clearData($_SERVER["REMOTE_ADDR"]) . ", " .
            "personal_street=" . clearData($_REQUEST["personal_street"]) . ", " .
            "personal_house_number=" . clearData($_REQUEST["personal_number"]) . ", " .
            "personal_postcode=" . clearData($_REQUEST["personal_postcode"]) . ", " .
            "personal_city=" . clearData($_REQUEST["personal_city"]) . ", " .
            "personal_state=" . clearData($_REQUEST["personal_state"]) . ", " .
            "average_transaction=" . clearData($_REQUEST["averageTransaction"]) . ", " .
            "annual_turnover=" . clearData($_REQUEST["annualTurnover"]) . ", " .
            "seasonal=" . clearData($_REQUEST["seasonal"]) . ", " .
            "location_type=" . clearData($_REQUEST["locationUse"]) . ", " .
            "kung_fu=" . clearData($_REQUEST["kungfuStyle"]) . " " .
          "WHERE merchant_id =" . clearData($_REQUEST["merch_id"]) . " " .
          "AND email =" . clearData($_REQUEST["email"]) . "";
        
    
        $insert = mysqli_query($conn, $sql);
        if ($insert) {
                $return['success'] = true;
                $return['query'] = $sql;
                $return['errorLog'] = "Res: " . $insert;
        } else {
                $return['errorLog'] = "Error: " . mysqli_error($conn);
        }
        if ($owntype== "partnership") {
                $insert = mysqli_query($conn, $sql);
                if ($insert) {
                    $return['success'] = true;
                    $return['errorLog'] = "Res: " . $insert;
                } else {
                    $return['errorLog'] = "Error: " . mysqli_error($conn);
                }
                $partTog=$_REQUEST["partnerToggle"];
                if ($partTog =="yes") {
                    //add and execute the insert on the partner table
                    $sql_partner = "INSERT INTO additional_partner_data(" . 
                    "FIRST_NAME, LAST_NAME, DOB, PHONE_NUMBER, PARTNER_STREET, PARTNER_HOUSE_NUMBER, PARTNER_POSTCODE, PARTNER_CITY, PARTNER_STATE, PARTNER_NR, MERCHANT_ID" . ") VALUES (" .
                    clearData($_REQUEST["partner_2_first_name"]). ", " .
                    clearData($_REQUEST["partner_2_last_name"]). ", " .
                    clearData($_REQUEST["partner_2_dob"]). ", " .
                    clearData($_REQUEST["partner_2_phone_number"]). ", " .
                    clearData($_REQUEST["partner_2_street"]). ", " .
                    clearData($_REQUEST["partner_2_house_number"]). ", " .
                    clearData($_REQUEST["partner_2_postcode"]). ", " .
                    clearData($_REQUEST["partner_2_city"]). ", " .
                    clearData($_REQUEST["partner_2_state"]). ", " .
                    clearData("2"). ", " .
                    clearData($_REQUEST["merch_id"]) . ")";
                    $return['query'] = $sql_partner;
                    $insertpart = mysqli_query($conn, $sql_partner);
                    if ($insertpart) {
                        $return['success'] = true;
                        $return['errorLog'] = "Res: " . $insertpart;
                    } else {
                        $return['errorLog'] = "Error: " . mysqli_error($conn);
                    }
                }
        }
        mysqli_close($conn);
} else {
        $return['errorLog'] = "Could not connect to the DB: " . mysqli_error($conn);
        exit;
}

echo json_encode($return);

?>