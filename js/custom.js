/*radio button new address phase3*/
$("#shippingtype1, #shippingtype2").on("click", function () {
    $("#newAddressSection").hide();
});
$("#shippingtype3").on("click", function () {
    $("#newAddressSection").show();
});

/*trading address section phase2*/
$("#trading2").on("click", function () {
    $("#tradingAddr").hide();
});
$("#trading1").on("click", function () {
    $("#tradingAddr").show();
});

/*shared address 2nd person phase2*/
$("#shared2-1").on("click", function () {
    $("#persDetail2subAddr").hide();
});
$("#shared2-2").on("click", function () {
    $("#persDetail2subAddr").show();
});

/*shared address 3th person phase2*/
$("#shared3-1").on("click", function () {
    $("#persDetail3subAddr").hide();
});
$("#shared3-2").on("click", function () {
    $("#persDetail3subAddr").show();
});

/*shared address 4th person phase2*/
$("#shared4-1").on("click", function () {
    $("#persDetail4subAddr").hide();
});
$("#shared4-2").on("click", function () {
    $("#persDetail4subAddr").show();
});

/*include partner info for partnership phase2*/
$("#partner1").on("click", function () {
    $("#personalDetail2").show();
});
$("#partner2").on("click", function () {
    $("#personalDetail2").hide();
});

/*ownership type phase2*/
/*company so 1 to 4 people*/
$("#business1").on("click", function () {
    $("#businessAddress").show();
    $("#ownershipType").show();
    $("#option2ndpartner").hide();
    /*one personal detail section as default*/
    $("#personalDetail2").hide();
    $("#personalDetail3").hide();
    $("#personalDetail4").hide();
});
/*partnership so just 2 people*/
$("#business2").on("click", function () {
    $("#businessAddress").show();
    $("#ownershipType").hide();
    $("#option2ndpartner").show();
    /*two fixed detail sections*/
    $("#personalDetail2").hide();
    $("#personalDetail3").hide();
    $("#personalDetail4").hide();
});
/*sole trader so just 1 person*/
$("#business3").on("click", function () {
    $("#businessAddress").hide();
    $("#ownershipType").hide();
    $("#option2ndpartner").hide();
    $("#personalDetail2").hide();
    $("#personalDetail3").hide();
    $("#personalDetail4").hide();
});
/*100 percent owned*/
$("#owner1").on("click", function () {
    $("#ownerNr").val(1);
    $("#personalDetail2").hide();
    $("#personalDetail3").hide();
    $("#personalDetail4").hide();
});

/*less than 25 percent owned*/
$("#owner3").on("click", function () {
    $("#ownerNr").val(1);
    $("#personalDetail2").hide();
    $("#personalDetail3").hide();
    $("#personalDetail4").hide();
});

/*shared ownership phase2*/
/*number of people selector*/
$(document).on('click', '.dropdown-menu li a', function () {
    var selOpt = $(this).text();
        switch (selOpt) { 
            case '2 Persons': 
            $("#owner2").prop("checked",true);
                $("#ownerNr").val(2);
            $("#personalDetail2").hide();
            $("#personalDetail3").hide();
            $("#personalDetail4").hide();
                break;
            case '3 Persons': 
            $("#owner2").prop("checked",true);
                $("#ownerNr").val(3);
            $("#personalDetail2").hide();
            $("#personalDetail3").hide();
            $("#personalDetail4").hide();
                break;
            case '4 Persons': 
            $("#owner2").prop("checked",true);
                $("#ownerNr").val(4);
            $("#personalDetail2").hide();
            $("#personalDetail3").hide();
            $("#personalDetail4").hide();
                break;      
            default:
                console.log("Nothing selected?");
        }
});

$("#quantity").change(function(){
	var value = $("#quantity").val();
  	var tax = 10.00;
    var cost = 90.00;
	var total= 0.00;
    subtotal = (value * cost).toFixed(2);

   taxtot=((tax * subtotal)/100).toFixed(2);
	total+=parseFloat(taxtot);
	total+=parseFloat(subtotal);
	total=total.toFixed(2);
    
	$('section.orders span#cardReadNr').text(value+" Tappr Card Reader");
	$('section.orders span#Subtotal').text("$"+subtotal);
	$('section.orders span#Taxtotal').text("$"+taxtot);
	$('section.orders span#Total').text("$"+total);
});

var cl = new CanvasLoader('canvasloader-container');
cl.setColor('#00ccff'); // default is '#000000'
cl.setShape('spiral'); // default is 'oval'
cl.setDiameter(52); // default is 40
cl.setDensity(29); // default is 40
cl.setRange(0.7); // default is 1.3
cl.setFPS(27); // default is 24
cl.hide(); // Hidden by default
