<?php
session_start();
$session_name = "tapprRegister_";

header('Content-type: application/json');

$return['error'] = true;

function clearData($val, $quotes = true) {
    $val = addslashes(trim($val));
    $val = str_replace("&", "", $val);

    return $quotes ? "'$val'" : $val;
}

$conn = oci_connect('tappr_reg', 'H3R9d3r9#', 'local_SID');
    if (!$conn) {
        $e = oci_error();
    $return["errorLog"] =  "Failed to connect to Oracle: " . $e['message'];
    }


    if ($conn) {
        $sql = 'UPDATE "merchant_data" SET ' .
          "BUSINESS_SERVICES=" . clearData($_REQUEST["business_category"]) . ", " .
            "BUSINESS_SUB_SERVICES=" . clearData($_REQUEST["business_sub_category"]) . ", " .
            "BUSINESS_TYPE=" . clearData($_REQUEST["businessType"]) . ", " .
            "BUSINESS_NAME=" . clearData($_REQUEST["business_name"]) . ", " .
            "BUSINESS_ABN_ACN=" . clearData($_REQUEST["business_abn_acn"]) . ", " .
            "FIRST_NAME=" . clearData($_REQUEST["first_name"]) . ", " .
            "LAST_NAME=" . clearData($_REQUEST["last_name"]) . ", " .
            "PHONE_NUMBER=" . clearData($_REQUEST["phone_number"]) . ", " .
            "USER_AGENT=" . clearData($_SERVER["HTTP_USER_AGENT"]) . ", " .
            "IP_ADDRESS=" . clearData($_SERVER["REMOTE_ADDR"]) . ", " .
            "PERSONAL_STREET=" . clearData($_REQUEST["address_street"]) . ", " .
            "PERSONAL_HOUSE_NUMBER=" . clearData($_REQUEST["address_number"]) . ", " .
            "PERSONAL_POSTCODE=" . clearData($_REQUEST["postal_code"]) . ", " .
            "PERSONAL_CITY=" . clearData($_REQUEST["address_city"]) . ", " .
            "PERSONAL_STATE=" . clearData($_REQUEST["address_state"]) . ", " .
            "AVERAGE_TRANSACTION=" . clearData($_REQUEST["averageTransaction"]) . ", " .
            "ANNUAL_TURNOVER=" . clearData($_REQUEST["annualTurnover"]) . ", " .
            "SEASONAL=" . clearData($_REQUEST["seasonal"]) . ", " .
            "LOCATION_TYPE=" . clearData($_REQUEST["locationUse"]) . ", " .
            "KUNG_FU=" . clearData($_REQUEST["kungfuStyle"]) . " " .
          "WHERE MERCHANT_ID =" . clearData($_REQUEST["merch_id"]) . " " .
          "AND EMAIL =" . clearData($_REQUEST["email"]) . "";

        $insert = oci_parse($conn, $sql);
        if(oci_execute($insert)){
            $url = 'http://deploy.mytappr.com/setup.php';

            $fields        = array(
                'merchantid' => urlencode(clearData($_REQUEST["merch_id"],false)),
                'password' => urlencode(clearData($_REQUEST["password"], false)),
                'email' => urlencode(clearData($_REQUEST["email"], false)),
                'businessname' => urlencode(clearData($_REQUEST["business_name"], false)),
                'first_name' => urlencode(clearData($_REQUEST["first_name"], false)),
                'last_name' => urlencode(clearData($_REQUEST["last_name"], false)),
                'trial' => urlencode(clearData("true", false))
            );
            $fields_string = "";
            foreach ($fields as $key => $value) {
                $fields_string .= $key . '=' . $value . '&';
            }
            rtrim($fields_string, '&');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //$result = stripslashes(urldecode(curl_exec($ch)));
            $result = stripslashes(curl_exec($ch));
            if ($result) {
                if ($fields["trial"]!='true') {
                    //production flow
                    $obj = json_decode($result);
                    /*$return["link"] = $result;
                    $return["fields"] = $fields_string;
                    $regex = '/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i';
                    preg_match_all($regex, $result, $matches);
                    $result                  = $matches[0][0];*/
                    $return["success"]       = true;
                    $return["url"]           = $fields["merchantid"];
                    $return["errorLog"]      = "All Good!";
                } else {
                    //trial flow
                    $obj = json_decode($result);
                    //$return["link"] = $result;
                    //$return["fields"] = $fields_string;
                    $return["url"]        = $obj->{'merchantId'};
                     /*$regex = '/t[0-9]+/';      //regex for merchant_id in trial flow
                    preg_match_all($regex, $result, $matches);
                    $result                  = $matches[0][0];*/
                    $return["success"]       = true;
                    $return["errorLog"]      = "All Good!";
                }
            } else {
                $return["errorLog"] = "[54] Server data connection error";
            }
            curl_close($ch);
        } else {
            $e = oci_error();
           $return["errorLog"] = "Error: " . $e['message'];
            exit;
        }
        //mysqli_close($conn);
    } else {
        $e = oci_error($conn);
       $return["errorLog"] = "Could not connect to the DB: " . $e['message']; //redundant?
        exit;
    }

echo json_encode($return);

?>
