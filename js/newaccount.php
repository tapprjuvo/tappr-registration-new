<?php
session_start();
$session_name = "juvoRegister_";

header('Content-type: application/json');

$return = array(
    "success" => false
);

function clearData($val, $quotes = true) {
    $val = addslashes(trim($val));
    $val = str_replace("&", "", $val);
    
    return $quotes ? "'$val'" : $val;
}

if ($_POST['register'] == "yes") {
    $conn = mysqli_connect("192.168.0.33", "root", "sysdba", "tappr_reg");
    
    /* 
    $conn = oci_connect('tappr_reg', 'H3R9d3r9#', 'local_SID');
    if (!$conn) {
        $e = oci_error();
    }
    */
    
    // Check connection
    if (mysqli_connect_errno()) {
        $return["errorLog"] =  "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
    if ($conn) {
        $result = mysqli_query($conn, "SELECT COUNT(*) as total FROM merchant_data");
        
        $data = mysqli_fetch_assoc($result);
        if (!$result) {
            $return["errorLog"] = "Could not successfully run query ($sql) from DB: " . mysqli_error($conn);
            exit;
        }
        //Generate a new ID
        /*  $stid = oci_parse($conn, 'SELECT COUNT(*) as total FROM merchant_data');
          oci_execute($stid);
          $row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS);
          $newId = $row['TOTAL']+1;
          */

        $newId      = $data['total'] + 1;
        $merchantId = "j" . $newId . "" . (round(time() / 3600));
        
        $sql = "INSERT INTO merchant_data (" . "id, merchant_id, email, country" . ") VALUES (" .
          clearData($newId) . ", " .
          clearData($merchantId) . ", " .
          clearData($_REQUEST["email"]) . ", " .
          clearData($_REQUEST["country"]) . ")";
        
        $insert = mysqli_query($conn, $sql);
        //$insert = oci_parse($conn, $sql);
        //if(oci_execute($insert)){
        if ($insert) {
            $return["success"] = true;
            $return["merchant_id"] = $merchantId;
        } else {
            $return["errorLog"] = "Error: " . mysqli_error($conn);
        }
        mysqli_close($conn);
    } else {
        $return["errorLog"] = "Could not connect to the DB: " . mysqli_error($conn); //redundant?
        exit;
    }
}

echo json_encode($return);

?>