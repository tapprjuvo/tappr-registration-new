<?php
error_reporting(E_ALL);
/*session_start();
$session_name = "juvoRegister_";*/

header('Content-type: application/json');

$return = array(
    "success" => false,
    "errorLog" => "",
    "query" => "",
    "querys" => ""
);

function clearData($val, $quotes = true) {
    $val = addslashes(trim($val));
    $val = str_replace("&", "", $val);
    
    return $quotes ? "'$val'" : $val;
}

$conn = mysqli_connect("192.168.0.33", "root", "sysdba", "tappr_reg");
    
    // Check connection
if (mysqli_connect_errno()) {
        $return['errorLog'] = "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
if ($conn) {
        $sql = "INSERT INTO credit_card_details (" .
            "CREDIT_CARD_TYPE, CREDIT_CARD_PAN, CREDIT_CARD_EXPIRY, MERCHANT_ID" . ") VALUES (" .
            clearData($_REQUEST["credit_card_type"]). ", " .
            clearData($_REQUEST["credit_card_pan"]). ", " .
            clearData($_REQUEST["credit_card_expiry"]). ", " .
            clearData($_REQUEST["merch_id"]) . ")";
        $return['query'] = $sql;
        
        $insert = mysqli_query($conn, $sql);
        
        if ($insert) {
            $return['success'] = true;
        } else {
            $return['errorLog'] = "Error: " . mysqli_error($conn);
        }
        
        $shipping_type=$_REQUEST["shippingtype"];
        //$shipping_type="HomeAddress";
        switch ($shipping_type) {
            case "NewAddress":
            $sql_ship = "INSERT INTO shipping_details (" . 
            "SHIPPING_STREET, SHIPPING_HOUSE_NUMBER, SHIPPING_POSTCODE, SHIPPING_CITY, SHIPPING_STATE, MERCHANT_ID" . ") VALUES (" .
            clearData($_REQUEST["shipping_street"]). ", " .
            clearData($_REQUEST["shipping_number"]). ", " .
            clearData($_REQUEST["shipping_postcode"]). ", " .
            clearData($_REQUEST["shipping_city"]). ", " .
            clearData($_REQUEST["shipping_state"]). ", " .
            clearData($_REQUEST["merch_id"]) . ")";
            $insertShip = mysqli_query($conn, $sql_ship);
        
            if ($insertShip) {
                $return['success'] = true;
            } else {
                $return['errorLog'] = "Error: " . mysqli_error($conn);
            }
            break;
            case "HomeAddress":
            $sql_ship = "INSERT INTO shipping_details (" . 
            "SHIPPING_STREET, SHIPPING_HOUSE_NUMBER, SHIPPING_POSTCODE, SHIPPING_CITY, SHIPPING_STATE, MERCHANT_ID" . ") SELECT " .
            "PERSONAL_STREET, PERSONAL_HOUSE_NUMBER, PERSONAL_POSTCODE, PERSONAL_CITY, PERSONAL_STATE, MERCHANT_ID FROM merchant_data_card ".
            "WHERE merchant_data_card.MERCHANT_ID=" . clearData($_REQUEST["merch_id"]);
            $insertShip = mysqli_query($conn, $sql_ship);
        
            if ($insertShip) {
                $return['success'] = true;
            } else {
                $return['errorLog'] = "Error: " . mysqli_error($conn);
            }
            break;
            case "BusinessAddress":
            $sql_ship = "INSERT INTO shipping_details (" . 
            "SHIPPING_STREET, SHIPPING_HOUSE_NUMBER, SHIPPING_POSTCODE, SHIPPING_CITY, SHIPPING_STATE, MERCHANT_ID" . ") SELECT " .
            "BUSINESS_STREET, BUSINESS_HOUSE_NUMBER, BUSINESS_POSTCODE, BUSINESS_CITY, BUSINESS_STATE, MERCHANT_ID ".
                " FROM merchant_data_card ".
            "WHERE merchant_data_card.MERCHANT_ID=" . clearData($_REQUEST["merch_id"]);
            $insertShip = mysqli_query($conn, $sql_ship);
        
            if ($insertShip) {
                $return['success'] = true;
            } else {
                $return['errorLog'] = "Error: " . mysqli_error($conn);
            }
            break;
        }
        
    $return['querys'] = $sql_ship;
        
        
    mysqli_close($conn);
} else {
    $return['errorLog'] = "Could not connect to the DB: " . mysqli_error($conn);
    exit;
}

echo json_encode($return);

?>