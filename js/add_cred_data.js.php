/*
<?php
session_start();
$session_name = "juvoRegister_";
?>
*/ 

/*************************************

	start_blur

*************************************/
function start_blur() {
    //show spinner and blur background
    cl.show();
    $("body").css("overflow", "hidden");
    $("#containerBlur").css("-webkit-filter", "blur(5px)");
    $("#containerBlur").css("-moz-filter", "blur(5px)");
    $("#containerBlur").css("-o-filter", "blur(5px)");
    $("#containerBlur").css("-ms-filter", "blur(5px)");
    $("#containerBlur").css("filter", "blur(5px)");
    $("#containerBlur :input").attr("disabled", true);
}

/*************************************

	stop_blur

*************************************/
function stop_blur() {
    //stop spinner and blur background
    cl.hide();
    $("body").css("overflow", "auto");
    $("#containerBlur").css("-webkit-filter", "");
    $("#containerBlur").css("-moz-filter", "");
    $("#containerBlur").css("-o-filter", "");
    $("#containerBlur").css("-ms-filter", "");
    $("#containerBlur").css("filter", "");
    $("#containerBlur :input").attr("disabled", false);
}

/*************************************

	check

*************************************/
function check() {
    var values = {
        //email: sessionStorage.getItem('user_mail'),
        //merch_id: sessionStorage.getItem('user_id'),
        merch_id: "ddd",
        quantity: $("select[name=quantity]").val(),
        credit_card_type: $("input[name=cardtype]:checked").val(),
        credit_card_pan: $("#credit_number").val(),
        credit_card_expiry: $("#credit_expiry_month").val() + "/" + $("#credit_expiry_year").val(),
        shippingtype: $("input[name=shippingtype]:checked").val(),
        shipping_number: $("#shipping_number").val(),
        shipping_street: $("#shipping_street").val(),
        shipping_state: $("#shipping_state").val(),
        shipping_city: $("#shipping_city").val(),
        shipping_postcode: $("#shipping_postcode").val()
    };
    //temporary solution for white spaces
     $('#registration *').filter(':input').each(function(key){
        if ( $.trim( $(key).val() ) == "" ) {
                $(key).val("");
                console.log(key);       
                return false;
        }
    });
    if(document.getElementById('registration').checkValidity()) {
          add_data(values);
      }
}
                                  
/*************************************

	add_data

*************************************/
                                  
function add_data(valuestoAdd) {
//spinner
    start_blur();
   // setTimeout(function() {
    $.ajax({
        type: "post",
        data: valuestoAdd,
        dataType: "json",
        url: "../js/addCredit.php"
    }).done(function (valuestoAdd, status) {
        console.log(valuestoAdd.success);
        console.log(status);
        if (valuestoAdd.success) {
            console.log("Update success!");
            $("#commit").prop("disabled", true);
            //stop spinner
            cl.hide();
            $( "body" ).css("overflow","auto");
            //data insert was correct
            window.location.href = "Tappr_Phase4.html";
        } else {
            console.log("Update failure!");
            console.log(valuestoAdd);
            stop_blur();
            $.confirm({
                                    text:  "<b>Error:</b> " + valuestoAdd.errorLog,
                                    title: "Error occurred during Insert",
                                    confirm: function(button) {
                                        //send the error via...
                                    },
                                    cancel: function(button) {
                                    },
                                    confirmButton: "Send Error Report",
                                    cancelButton: "No",
                                    post: true
                                });
            //data insert was incorrect
            //highlght the problem visually
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        console.log(textStatus);
        console.log(errorThrown);
        //trouble with the php
        stop_blur();
            $.confirm({
                                    text:  "<b>Error:</b> " + textStatus,
                                    title: "Error occurred during Processing",
                                    confirm: function(button) {
                                    },
                                    cancel: function(button) {
                                    },
                                    confirmButton: "Send Error Report",
                                    cancelButton: "No",
                                    post: true
                                });
    });
//}, 2000);
}