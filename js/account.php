<?php
session_start();
$session_name = "juvoRegister_";

ini_set( 'display_errors', '1' );
error_reporting (E_ALL &~ E_NOTICE &~ E_DEPRECATED &~ E_WARNING);


//if($_POST["dCheck"] != $_SESSION[$session_name."ap_id"]){

//	$return["error"] = "[92] Humanity check failed";

//}else{

	$return = array("success" => false);

	function clearData($val, $quotes = true){

		global $mysqli;

		$val = $mysqli->real_escape_string(trim($val));

		return $quotes ? "'$val'" : $val;

	}

	$mysqli = new mysqli("127.0.0.1","root","passwordroot", "registrations");

	if ($mysqli->connect_error) {

		$return["error"] = "[34] Server data connection error";

		//die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);

	}else{

		/*

		$result = $mysqli->query("show columns from user_data");

		while($obj = $result->fetch_object()){

			echo nl2br(print_r($obj, true))."<br>";

		}
		*/

		if( $_POST['register'] == "yes"){

			$sql = "".
				"INSERT INTO registrations.user_data (\n".
					"email, country, \n".
					"business_category, business_type, \n".
					"business_name, business_abn_acn, business_trading_type, business_revenue, employee_num, \n".
					"kung_fu, \n".
					"first_name, last_name, dob, phone_number,\n".
					"user_agent, win_width, win_height, ip_address\n".
				") VALUES (\n".
					clearData($_REQUEST["email"]).", \n".
					clearData($_REQUEST["country"]).", \n\n".

					clearData($_REQUEST["services"]).", \n".
					clearData($_REQUEST["retail_type"]).", \n\n".

					clearData($_REQUEST["business_name"]).", \n".
					clearData($_REQUEST["business_abn_acn"]).", \n".
					clearData($_REQUEST["bus_type"]).", \n".
					clearData($_REQUEST["annual_turnover"]).", \n".
					clearData($_REQUEST["employees"]).", \n\n".

					clearData($_REQUEST["kung_fu"]).", \n\n".

					clearData($_REQUEST["first_name"]).", \n".
					clearData($_REQUEST["last_name"]).", \n".
					clearData($_REQUEST["dob"]).", \n".
					clearData($_REQUEST["mobile"]).",\n".

					clearData($_SERVER["HTTP_USER_AGENT"]).",\n".
					clearData($_REQUEST["win_width"]).",\n".
					clearData($_REQUEST["win_height"]).",\n".
					clearData($_SERVER["REMOTE_ADDR"])."\n".

				");\n".
				"SELECT LAST_INSERT_ID() INTO @lastID ;\n".
				"UPDATE registrations.user_data SET merchant_id = CONCAT('j', @lastID, ROUND(UNIX_TIMESTAMP()/3600)) WHERE id = @lastID LIMIT 1;\n".
				"SELECT merchant_id AS merchantid FROM registrations.user_data WHERE id = @lastID;";
				//"SELECT merchantid FROM user_data WHERE id = @lastID LIMIT 1";

				$query = $mysqli->multi_query($sql);

				if(!$query){

					$return["error"] = "[28] Server data connection error - ".$mysqli->error;
					//echo $mysqli->error;

				}else{

					//echo "<br>".nl2br($sql)."<br>";
					//print_r(mysqli_error_list($mysqli));

					$mysqli->next_result();
					$mysqli->next_result();
					$mysqli->next_result();

					if ($result = $mysqli->use_result()) {

						$obj = $result->fetch_object();
						$merchantid = $obj->merchantid;
						//echo "merchantid = $merchantid";

						if($merchantid == ""){

							//echo $sql;

							$return["error"] = "[73] Server data connection error";
							//$return["sql"] = $sql;

						}else{

							$url = 'http://srv01.juvo.net.au/setup.php';

							$fields = array(
								'email' => urlencode(clearData($_REQUEST["email"], false)),
								'merchantid' => urlencode($merchantid),
								'password' => urlencode(clearData($_REQUEST["password"], false)),

								'business_category' => urlencode(clearData($_REQUEST["services"], false)),
								'business_type' => urlencode(clearData($_REQUEST["retail_type"], false)),

								'business_name' => urlencode(clearData($_REQUEST["business_name"], false)),
								'business_abn_acn' => urlencode(clearData($_REQUEST["business_abn_acn"], false)),
								'business_trading_type' => urlencode(clearData($_REQUEST["bus_type"], false)),
								'business_revenue' => urlencode(clearData($_REQUEST["annual_turnover"], false)),
								'employee_num' => urlencode(clearData($_REQUEST["employees"], false)),

								'kung_fu' => urlencode(clearData($_REQUEST["kung_fu"], false)),

								'first_name' => urlencode(clearData($_REQUEST["first_name"], false)),
								'last_name' => urlencode(clearData($_REQUEST["last_name"], false)),
								'dob' => urlencode(clearData($_REQUEST["dob"], false)),
								'phone_number' => urlencode(clearData($_REQUEST["mobile"], false)),

								"user_agent" => urlencode(clearData($_SERVER["HTTP_USER_AGENT"], false)),
								"win_height" => urlencode(clearData($_REQUEST["win_height"], false)),
								"win_width" => urlencode(clearData($_REQUEST["win_width"], false)),
								"ip_address" => urlencode(clearData($_SERVER["REMOTE_ADDR"], false)),
							);

							//$return["merchantID"] = $merchantid;

							$fields_string = "";

							foreach($fields as $key=>$value) {

								$fields_string .= $key.'='.$value.'&';

							}

							rtrim($fields_string, '&');

							$ch = curl_init();


							curl_setopt($ch,CURLOPT_URL, $url);
							curl_setopt($ch,CURLOPT_POST, count($fields));
							curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

							//$result = stripslashes(urldecode(curl_exec($ch)));
							$result = stripslashes(curl_exec($ch));

							if($result){

								//echo $result;

								//$return["link"] = $result;
								//$return["fields"] = $fields_string;

								$regex = '/\b(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)[-A-Z0-9+&@#\/%=~_|$?!:,.]*[A-Z0-9+&@#\/%=~_|$]/i';
								preg_match_all($regex, $result, $matches);
								$result = $matches[0][0];

								//echo "<br>Curl is good<br>";
								//print_r($result);
								//$return["success"] = false;
								$return["success"] = true;
								$return["result"] = $result;

							}else{

								$return["error"] = "[54] Server data connection error";
								//echo "<br>Curl is bad<br>";
								//echo curl_error($ch);

							}

							curl_close($ch);

						}

					}else{

						echo "Unable to get a result";

					}
				}


			/*
			$first_name = mysql_real_escape_string($_POST['first_name']);
			$last_name = mysql_real_escape_string($_POST['last_name']);
			$phone_number = mysql_real_escape_string($_POST['phone_number']);
			$country = mysql_real_escape_string($_POST['country']);
			$business_category = mysql_real_escape_string($_POST['business_category']);
			$business_type = mysql_real_escape_string($_POST['business_type']);
			$business_name = mysql_real_escape_string($_POST['business_name']);
			$business_trading_type = mysql_real_escape_string($_POST['business_trading_type']);
			$business_abn_business_abn_acn = mysql_real_escape_string($_POST['business_abn_business_abn_acn']);
			$business_revenue = mysql_real_escape_string($_POST['business_revenue']);
			$juvo_usage_plan = mysql_real_escape_string($_POST['juvo_usage_plan']);
			$registered_interest = mysql_real_escape_string($_POST['registered_interest']);


			$errors = [];

			if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
			$errors[] = "Email address is not valid";
			}

			if( count($errors) > 0 ){
			print_r($errors);
			exit();
			}

			$email = mysql_real_escape_string($_POST['email']);

			mysql_connect("127.0.0.1","root","passwordroot")or die(mysql_error());
			mysql_select_db("registrations")or die(mysql_error());
			mysql_query("insert into user_data (first_name,last_name,email,phone_number,country,business_category,business_type,business_name,business_trading_type,business_abn_business_abn_acn,business_revenue,juvo_usage_plan,registered_interest)
			values
			('$first_name','$last_name','$email','$phone_number','$country','$business_category','$business_type','$business_name','$business_trading_type','$business_abn_business_abn_acn','$business_revenue','$juvo_usage_plan','$registered_interest')")or die(mysql_error());


			$aid = mysql_insert_id();
			$merchantId = "j".$aid."".(round(time() / 3600));
			mysql_query("update user_data set merchant_id = '$merchantId' where id = '$aid'")or die(mysql_error());
			$url = 'http://srv01.juvo.net.au/setup.php';
			$fields = array('last_name' => urlencode($last_name),
			'first_name' => urlencode($first_name),
			'password' => urlencode($password),
			'email' => urlencode($email),
			'merchantid' => urlencode($merchantId));


			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');

			$ch = curl_init();


			curl_setopt($ch,CURLOPT_URL, $url);
			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

			$result = curl_exec($ch);
			print_r($result);

			curl_close($ch);
			exit();
			*/
		}
	}
//} //Removed "humanity check"

echo json_encode($return);

mysqli_close($mysqli);
