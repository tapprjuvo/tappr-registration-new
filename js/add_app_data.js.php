<?php
session_start();
$session_name = "juvoRegister_";
require_once 'iOS.js';
?>


/*************************************

	start_blur

*************************************/
function start_blur() {
		//show spinner and blur background
$('#opContainer').show();
		cl.show();
		$("body").css("overflow", "hidden");
		$("#containerBlur").css("-webkit-filter", "blur(5px)");
		$("#containerBlur").css("-moz-filter", "blur(5px)");
		$("#containerBlur").css("-o-filter", "blur(5px)");
		$("#containerBlur").css("-ms-filter", "blur(5px)");
		$("#containerBlur").css("filter", "blur(5px)");
		$("#containerBlur :input").attr("disabled", true);
}

/*************************************

	stop_blur

*************************************/
function stop_blur() {
		//stop spinner and blur background
$('#opContainer').hide();
		cl.hide();
		$("body").css("overflow", "auto");
		$("#containerBlur").css("-webkit-filter", "");
		$("#containerBlur").css("-moz-filter", "");
		$("#containerBlur").css("-o-filter", "");
		$("#containerBlur").css("-ms-filter", "");
		$("#containerBlur").css("filter", "");
		$("#containerBlur :input").attr("disabled", false);
}

/*************************************

	check

*************************************/
function check(e) {
		//e.preventDefault();
		//temporary solution for white spaces
		$('#registration *').filter(':input').each(function(key){
				if ( $.trim( $(key).val() ) == "" ) {
								$(key).val("");
								return false;
				}
		});
		if(document.getElementById('registration').checkValidity()) {
					add_data();
			}
		else {
				$(document).scrollTop( $("#failFields").offset().top );
				$('#registration *').filter(':input').each(function(){
				if ($(this).is(":invalid")) {
						$(this).css("border", "3px solid #f00");
				}
		});
		}
}

/*************************************

	add_data

*************************************/

function add_data() {
		//spinner
		if(isiOS) {
		window.location = "juvo://clickedRegisterButton";
	} else {
				start_blur();
	}
		//setTimeout(function() {
		var values = {
				email: sessionStorage.getItem('user_mail'),
				merch_id: sessionStorage.getItem('user_id'),
				password: sessionStorage.getItem('user_psswd'),
				business_category: $("#business_category").val(),
				business_sub_category: $("#business_sub_category").val(),
				businessType: $("input[name=businessType]:checked").val(),
				business_abn_acn: $("input#business_abn_acn").val(),
				business_name: $("input#business_name").val(),
				last_name: $("#last_name").val(),
				first_name: $("#first_name").val(),
				phone_number: $("#phone_number").val(),
				address_number: $("#address_number").val(),
				address_street: $("#address_street").val(),
				address_state: $("#address_state").val(),
				address_city: $("#address_city").val(),
				postal_code: $("#postal_code").val(),
				averageTransaction: $("#averageTransaction").val(),
				annualTurnover: $("#annualTurnover").val(),
				seasonal: $("input[name=seasonal]:checked").val(),
				locationUse: $("input[name=locationUse]:checked").val(),
				kungfuStyle: $("select[name=kungfuStyle]").val()
		};
		$.ajax({
				type: "post",
				data: values,
				dataType: "json",
				url: "../js/addAppData.php"
		}).done(function (values, status) {
				console.log(values);
				console.log(values.errorLog);
				console.log(status);
				if (data.success) {
				onRegistrationSuccessForiOS(values);
						console.log("Update success!");
						$("#commit").prop("disabled", true);
						//stop spinner
						stop_blur();
						if (window.sessionStorage){
								sessionStorage.setItem("url", values.url) //store data using setItem()
						}
						//data insert was correct
						window.location.href = "../Tappr_Final.html";

				} else {
						onRegistrationFailureForiOS();
						console.log("Update failure!");
						console.log(data);
						stop_blur();
						$.confirm({
																		text:  "<b>Error:</b> " + data.errorLog,
																		title: "Error occurred during Insert",
																		confirm: function(button) {
																				//send the error via...
																		},
																		cancel: function(button) {
																		},
																		confirmButton: "Send Error Report",
																		cancelButton: "No",
																		post: true
																});
						//data insert was incorrect
						//highlght the problem visually
				}
		}).fail(function (jqXHR, textStatus, errorThrown) {
				console.log(textStatus);
				console.log(errorThrown);
				//trouble with the php
				stop_blur();
						$.confirm({
																		text:  "<b>Error:</b> " + textStatus,
																		title: "Error occurred during Processing",
																		confirm: function(button) {
																		},
																		cancel: function(button) {
																		},
																		confirmButton: "Send Error Report",
																		cancelButton: "No",
																		post: true
																});
		});
//}, 2000);
}
