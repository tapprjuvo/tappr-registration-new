/*************************************

	password input

*************************************/
$("#commit").on("click",function(){
	$('body').css('overflow','auto');
});

$("#password input").on("change",function(){

	chech_fields("password");

});

/*************************************

	step 1 fields

*************************************/

$("#password input").on("blur", function(e){

	console.log("a");

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			chech_fields($(this).attr("name"));
			break;
	}


});

$("#email input").on("blur", function(e){
	var len = $(this).val();
	$.post("/validation/validate.php",{"field[EMAIL]":$(this).val()},function(data){

			if(data=='false'){
					if(len==''){
						$(".validation-response","#email").text('Value required').fadeIn();
					}else{
				$(".validation-response","#email").text("The email doesn't seem to be valid").fadeIn();
			}
			}else if(data=='taken'){
				$(".validation-response","#email").text("The email is already registered with Tappr").fadeIn();
			}else{
				$(".validation-response","#email").text('').fadeIn();
			}
	});

if($(this).val()==''){
	$(".validation-response","#email").text('Value required').fadeIn();
}

});


$("#email input, #password input").on("change",function(){

	chech_fields($(this).attr("name"));

});

$("#country select").on("change",function(){

	chech_fields($(this).attr("name"));

});

/*************************************

	checkEmail

*************************************/

function checkEmail(emailStr){

	var emailPat=/^(.+)@(.+)$/;
	var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
	var validChars="\[^\\s"+specialChars+"\]";
	var quotedUser="(\"[^\"]*\")";
	var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
	var atom=validChars+'+';
	var word="("+atom+"|"+quotedUser+")";
	var userPat=new RegExp("^"+word+"(\\."+word+")*$");
	var domainPat=new RegExp("^"+atom+"(\\."+atom+")*$");
	var matchArray=emailStr.match(emailPat);
	var error_message;

	if(matchArray==null){

		return {correct: false, response: "Check @ and .'s"};

	}

	var user=matchArray[1];
	var domain=matchArray[2];

	if(user.match(userPat)==null){

		return {correct: false, response: "The email doesn't seem to be valid"};

	}

	var IPArray=domain.match(ipDomainPat);

	if(IPArray!=null){

		for(var i=1;i<=4;i++){

			if(IPArray[i]>255){

				return {correct: false, response: "Destination IP address is invalid"};

			}
		}

		return {correct:true};
	}

	var domainArray=domain.match(domainPat);

	if(domainArray==null){

		return {correct: false, response: "Domain name doesn't seem to be valid"};

	}

	var atomPat=new RegExp(atom,"g");
	var domArr=domain.match(atomPat);
	var len=domArr.length;

	if(domArr[domArr.length-1].length<2||domArr[domArr.length-1].length>3){

		return {correct: false, response: "Must end with 3 letter domain or 2 letter country"};

	}

	if(len<2){



	}

	return {correct:true};

}

/*************************************

	chech_fields

*************************************/

var valid = ['top'];

function chech_fields(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];

	//console.log(!check ? "all" : check);

	valid = is_valid($("#email input"), ["empty", "email"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("email", "input", valid, check, init);

	valid = is_valid($("#password input"), ["empty", "numbers", 8]);


	if (!valid.correct) {

		correct = false;

	}

	show_response("password", "input", valid, check, init);

	valid = {correct: $( "#country option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please choose a country";

	}

	show_response("country", "option:selected", valid, check, init);


	return correct;

}