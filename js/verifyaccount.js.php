<?php
session_start();
$session_name = "juvoRegister_";
?>

$.getScript("js/sha256.js");

//useless at the moment as the selector is locked to Australia-->
//$("#locale_code").on("change", function () {});

//during digitation the Response string will disappear-->

$('#password').bind('input', function () {
    $("span#password.validation-response").text('').fadeOut();
});

$('#email').bind('input', function () {
    $("span#email.validation-response").text('').fadeOut();
});

//when password loses focus check the consistency-->
$("#password").on("blur", function () {
    check_fields($(this).attr("name"));
});

//when email loses focus check the consistency-->
$("#email").on("blur", function () {
    var len = $(this).val();
    $.post("validation/validate.php", {
        "field[EMAIL]": $(this).val()
    }, function (data) {
        if (data == 'false') {
            if (len == '') {
                $("span#email.validation-response").text('Value required').fadeIn();
            } else {
                $("span#email.validation-response").text("The email doesn't seem to be valid").fadeIn();
            }
        } else if (data == 'taken') {
            $("span#email.validation-response").text("The email is already registered with Tappr").fadeIn();
            $("#commit").prop("disabled", true);
        } else {
            $("span#email.validation-response").text('').fadeIn();
        }
    });

    if ($(this).val() == '') {
        $("span#email.validation-response").text('Value required').fadeIn();
    }

});

//when password loses focus check the consistency-->
$("#password, #email").on("change", function () {
    check_fields($(this).attr("name"));
});

$("#commit").on("click", function () {
    $('body').css('overflow', 'auto');
});



function show_response(tgt, type, valid, check, init) {

    //console.log(tgt + " - " + ((!init || $.trim($("#" + tgt + " " + type).val()) != "") ? "show" : "hide"));
    if ((!check || check == tgt) && (!init || $.trim($("#" + tgt + " " + type).val()) != "")) {
        if (!valid.correct) {
            $("span#" + tgt + ".validation-response").text(valid.response).fadeIn();
        } else {
            $("span#" + tgt + ".validation-response").fadeOut(function () {
                $(this).empty();
            });
        }
    }
}

/*************************************

	start_blur

*************************************/

function start_blur() {
    cl.show();
    $( "body" ).css("overflow","hidden");
    $( "#containerC" ).css("-webkit-filter","blur(5px)");
    $( "#containerC" ).css("-moz-filter","blur(5px)");
    $( "#containerC" ).css("-o-filter","blur(5px)");
    $( "#containerC" ).css("-ms-filter","blur(5px)");
    $( "#containerC" ).css("filter","blur(5px)");
    $("#containerC :input").attr("disabled", true);
}

/*************************************

	stop_blur

*************************************/

function stop_blur() {
    cl.hide();
        $( "body" ).css("overflow","auto");
        $( "#containerC" ).css("-webkit-filter","");
        $( "#containerC" ).css("-moz-filter","");
        $( "#containerC" ).css("-o-filter","");
            $( "#containerC" ).css("-ms-filter","");
            $( "#containerC" ).css("filter","");
            $("#containerC :input").attr("disabled", false);
}

/*************************************

	checkEmail

*************************************/

function checkEmail(emailStr) {
    var emailPat = /^(.+)@(.+)$/;
    var specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
    var validChars = "\[^\\s" + specialChars + "\]";
    var quotedUser = "(\"[^\"]*\")";
    var ipDomainPat = /^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
    var atom = validChars + '+';
    var word = "(" + atom + "|" + quotedUser + ")";
    var userPat = new RegExp("^" + word + "(\\." + word + ")*$");
    var domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
    var matchArray = emailStr.match(emailPat);
    var error_message;
    if (matchArray == null) {
        return {
            correct: false,
            response: "Check @ and .'s"
        };
    }
    var user = matchArray[1];
    var domain = matchArray[2];
    if (user.match(userPat) == null) {
        return {
            correct: false,
            response: "The email doesn't seem to be valid"
        };
    }
    var IPArray = domain.match(ipDomainPat);
    if (IPArray != null) {
        for (var i = 1; i <= 4; i++) {
            if (IPArray[i] > 255) {
                return {
                    correct: false,
                    response: "Destination IP address is invalid"
                };
            }
        }
        return {
            correct: true
        };
    }
    var domainArray = domain.match(domainPat);
    if (domainArray == null) {
        return {
            correct: false,
            response: "Domain name doesn't seem to be valid"
        };
    }
    var atomPat = new RegExp(atom, "g");
    var domArr = domain.match(atomPat);
    var len = domArr.length;
    if (domArr[domArr.length - 1].length < 2 || domArr[domArr.length - 1].length > 3) {
        return {
            correct: false,
            response: "Must end with 3 letter domain or 2 letter country"
        };
    }
    return {
        correct: true
    };
}

/*************************************

	is_valid

*************************************/

function is_valid(fld, tests) {

    var val = $.trim($(fld).val());
    var field_type = arguments[1] ? arguments[1] : "text";
    var num_test = /^(?=.*\d.*\d).*$/;
    var symbol = /^(?=.*[!@#$%^&*]).*$/;
    //console.log(fld);
    var correct = true;
    var i, response = "",
        count = 0;
    //console.clear();
    for (i = 0; i <= tests.length; i++) {
        count++;
        if (typeof (tests[i]) == "number") {
            correct = val.length >= tests[i];
            if (!correct) {
                response = "Must be at least 8 characters long";
            }
        } else {
            switch (tests[i]) {
            case "human":
                correct = false;
                if (val == 12) {
                    correct = true;
                }
                if (!correct) {
                    response = "Are you sure you're not a robot?";
                }
                break;
            case "empty":
                correct = val.length > 0;
                if (!correct) {
                    response = "Value required";
                }
                break;
            case "email":
                correct = checkEmail(val);
                if (!correct.correct) {
                    response = correct.response;
                }
                correct = correct.correct;
                break;
            case "numbers":
                correct = num_test.test(val);
                if (!correct) {
                    response = "Must contain at least 2 numbers";
                }
                break;
            }
        }
        if (!correct) {
            break;
        }
    }
    return {
        correct: correct,
        response: response
    };
}

/*************************************

	create_account

*************************************/

function create_account() {
//spinner
    start_blur();
    //setTimeout(function() {
    var data = {
        register: "yes",
        email: $("#email").val(),
        country: $("#locale_code").val()
    };
    $.ajax({
        type: "post",
        data: data,
        dataType: "json",
        url: "js/newaccount.php"
    }).done(function (data) {
        console.log(data.success);
        if (data.success) {
            console.log("Insert success!");
            $("#commit").prop("disabled", true);
            //stop spinner
            cl.hide();
            $( "body" ).css("overflow","auto");
            if (window.sessionStorage){
                sessionStorage.setItem("user_id", data.merchant_id) //store data using setItem()
                sessionStorage.setItem("user_mail", $("#email").val()) //store data using setItem()
                sessionStorage.setItem("user_psswd", sha256_digest($("#password").val())) //store data using setItem()
            }
            //data insert was correct
            window.location.href = "Tappr_Selection.html";
        } else {
            console.log("Insert failure!");
            console.log(data.error);
            stop_blur();
            $.confirm({
                                    text:  "<b>Error:</b> " + data.error,
                                    title: "Error occurred",
                                    confirm: function(button) {
                                        //send the error via...
                                    },
                                    cancel: function(button) {
                                    },
                                    confirmButton: "Send Error Report",
                                    cancelButton: "No",
                                    post: true
                                });
            //data insert was incorrect
            //highlght the problem visually
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        console.log(textStatus);
        console.log(errorThrown);
        //trouble with the php
        stop_blur();
            $.confirm({
                                    text:  "<b>Error:</b> " + textStatus + " " + errorThrown,
                                    title: "Error occurred",
                                    confirm: function(button) {
                                    },
                                    cancel: function(button) {
                                    },
                                    post: true
                                });
    });
//}, 3000);
}



function check_fields() {
    var correct = true,
        valid;
    var check = arguments[0];
    var init = arguments[1];

    //console.log(check + " / " + init);

    valid = is_valid($("#email"), ["empty", "email"]);

    if (!valid.correct) {
        correct = false;
    }

    show_response("email", "input", valid, check, init);

    valid = is_valid($("#password"), ["empty", "numbers", 8]);

    if (!valid.correct) {
        correct = false;
    }

    show_response("password", "input", valid, check, init);

    /*valid = {correct: $( "#country option:selected" ).val() != ""};
	if (!valid.correct) {
		correct = false;
		valid.response = "Please choose a country";
	}
	show_response("country", "option:selected", valid, check, init);*/

    var str = $( "span#email.validation-response" ).text();

    if (correct && str != "The email is already registered with Tappr") {
        $("#commit").prop("disabled", false);
    } else {
        $("#commit").prop("disabled", true);
    }
    return correct;

}