/*
<?php
?>
*/ 

/*************************************

	start_blur

*************************************/
function start_blur() {
    //show spinner and blur background
    cl.show();
    $("body").css("overflow", "hidden");
    $("#containerBlur").css("-webkit-filter", "blur(5px)");
    $("#containerBlur").css("-moz-filter", "blur(5px)");
    $("#containerBlur").css("-o-filter", "blur(5px)");
    $("#containerBlur").css("-ms-filter", "blur(5px)");
    $("#containerBlur").css("filter", "blur(5px)");
    $("#containerBlur :input").attr("disabled", true);
}

/*************************************

	stop_blur

*************************************/
function stop_blur() {
    //stop spinner and blur background
    cl.hide();
    $("body").css("overflow", "auto");
    $("#containerBlur").css("-webkit-filter", "");
    $("#containerBlur").css("-moz-filter", "");
    $("#containerBlur").css("-o-filter", "");
    $("#containerBlur").css("-ms-filter", "");
    $("#containerBlur").css("filter", "");
    $("#containerBlur :input").attr("disabled", false);
}

/*************************************

	check

*************************************/
function check() {
    var bustype = $("input[name=businessType]:checked").val();
    switch (bustype) {
    case "company":
            //I initialize the array with the starting fields
        var values = {
            email: "abc@abc.com",
            merch_id: "ddd",
            /*email: sessionStorage.getItem('user_mail'),
            merch_id: sessionStorage.getItem('user_id'),*/
            business_category: $("#business_category").val(),
            business_sub_category: $("#business_sub_category").val(),
            businessType: $("input[name=businessType]:checked").val(),
            business_abn_acn: $("input#business_abn_acn").val(),
            business_name: $("input#business_name").val(),
            business_number: $("#business_number").val(),
            business_street: $("#business_street").val(),
            business_state: $("#business_state").val(),
            business_city: $("#business_city").val(),
            business_postcode: $("#business_postcode").val()
        };
        //if the trading address radio is selected I append the trading fields
        var tradingToggle = $("input[name=trading]:checked").val();
        if (tradingToggle == "yes") {
            var tradValues = {trading_number: $("#trading_number").val(),
                trading_street: $("#trading_street").val(),
                trading_state: $("#trading_state").val(),
                trading_city: $("#trading_city").val(),
                trading_postcode: $("#trading_postcode").val()
            };
            $.extend(values, tradValues);
        }
        //then I append the personal details
        var persValues = {
            ownershipType: $("input[name=ownershipType]:checked").val(),
            last_name: $("#personal_last_name").val(),
            first_name: $("#personal_first_name").val(),
            pers_dob: $("#personal_birthdate_day").val() + "/" + $("#personal_birthdate_month").val() + "/" + $("#personal_birthdate_year").val(),
            phone_number: $("#personal_phone_number").val(),
            driverlicense: $("#driverlicense").val(),
            personal_number: $("#personal_number").val(),
            personal_street: $("#personal_street").val(),
            personal_state: $("#personal_state").val(),
            personal_city: $("#personal_city").val(),
            personal_postcode: $("#personal_postcode").val(),
            ownerNr: $("#ownerNr").val(),
            averageTransaction: $("#averageTransaction").val(),
            annualTurnover: $("#annualTurnover").val(),
            seasonal: $("input[name=seasonal]:checked").val(),
            locationUse: $("input[name=locationUse]:checked").val(),
            kungfuStyle: $("select[name=kungfuStyle]").val()
                    };
        $.extend(values, persValues);
        //if they select the partner selector i first append those input fields
        /*
        if (ownType == "25+") {
            //in case of multiple partners I prepare the objects to be added to the main one
            var part2Values = {
                        partner_2_first_name: $("#partner_2_first_name").val(),
                        partner_2_last_name : $("#partner_2_last_name").val(),
                        partner_2_phone_number: $("#partner_2_phone_number").val(),
                        partner_2_dob: $("#partner_2_birthdate_day").val() + "/" + $("#partner_2_birthdate_month").val() + "/" + $("#partner_2_birthdate_year").val()
                };
            //as the address is optional I surround it with an if
            var partner2Addr = $("input[name=shared2]:checked").val();
            if (partner2Addr == "no") {
                var partAddr2Values = {partner_2_house_number: $("#partner_2_house_number").val(),
                            partner_2_street : $("#partner_2_street").val(),
                            partner_2_city: $("#partner_2_city").val(),
                    partner_2_postcode: $("#partner_2_postcode").val(),
                    partner_2_postcode: $("#partner_2_postcode").val(),
                    partner_2_state: $("#partner_2_state").val(),
                };
                $.extend(part2Values, partAddr2Values);
            }
            var part3Values = {
                        partner_3_first_name: $("#partner_3_first_name").val(),
                        partner_3_last_name : $("#partner_3_last_name").val(),
                        partner_3_phone_number: $("#partner_3_phone_number").val(),
                        partner_3_dob: $("#partner_3_birthdate_day").val() + "/" + $("#partner_3_birthdate_month").val() + "/" + $("#partner_3_birthdate_year").val()
                };
            var partner3Addr = $("input[name=shared3]:checked").val();
            if (partner3Addr == "no") {
                var partAddr3Values = {partner_3_house_number: $("#partner_3_house_number").val(),
                            partner_3_street : $("#partner_3_street").val(),
                            partner_3_city: $("#partner_3_city").val(),
                    partner_3_postcode: $("#partner_3_postcode").val(),
                    partner_3_postcode: $("#partner_3_postcode").val(),
                    partner_3_state: $("#partner_3_state").val(),
                };
                $.extend(part3Values, partAddr3Values);
            }
            var part4Values = {
                        partner_4_first_name: $("#partner_4_first_name").val(),
                        partner_4_last_name : $("#partner_4_last_name").val(),
                        partner_4_phone_number: $("#partner_4_phone_number").val(),
                        partner_4_dob: $("#partner_4_birthdate_day").val() + "/" + $("#partner_4_birthdate_month").val() + "/" + $("#partner_4_birthdate_year").val()
                };
            var partner4Addr = $("input[name=shared4]:checked").val();
            if (partner4Addr == "no") {
                var partAddr4Values = {partner_4_house_number: $("#partner_4_house_number").val(),
                            partner_4_street : $("#partner_4_street").val(),
                            partner_4_city: $("#partner_4_city").val(),
                    partner_4_postcode: $("#partner_4_postcode").val(),
                    partner_4_postcode: $("#partner_4_postcode").val(),
                    partner_4_state: $("#partner_4_state").val(),
                };
                $.extend(part4Values, partAddr4Values);
            }
            
            switch (ownerNr) {
                    case '2':
                    $.extend(values, part2Values);
                    break;
                    case '3':
                    $.extend(values, part2Values);
                    $.extend(values, part3Values);
                    break;
                    case '4':
                    $.extend(values, part2Values);
                    $.extend(values, part3Values);
                    $.extend(values, part4Values);
                    break;
            }
        }*/
        //finally I append the last fields
        break;
    case "partnership":
        //I initialize the array with the starting fields
        var values = {
            email: "abc@abc.com",
            merch_id: "acd",
            /*email: sessionStorage.getItem('user_mail'),
            merch_id: sessionStorage.getItem('user_id'),*/
            business_category: $("#business_category").val(),
            business_sub_category: $("#business_sub_category").val(),
            businessType: $("input[name=businessType]:checked").val(),
            business_abn_acn: $("input#business_abn_acn").val(),
            business_name: $("input#business_name").val(),
            business_number: $("#business_number").val(),
            business_street: $("#business_street").val(),
            business_state: $("#business_state").val(),
            business_city: $("#business_city").val(),
            business_postcode: $("#business_postcode").val()
        };
        //if the trading address radio is selected I append the trading fields
        var tradingToggle = $("input[name=trading]:checked").val();
        if (tradingToggle == "yes") {
            var tradValues = {trading_number: $("#trading_number").val(),
                trading_street: $("#trading_street").val(),
                trading_state: $("#trading_state").val(),
                trading_city: $("#trading_city").val(),
                trading_postcode: $("#trading_postcode").val()
            };
            $.extend(values, tradValues);
        }
            var partnerToggle = $("input[name=partnerinfo]:checked").val();
        //then I append the personal details
        var persValues = {last_name: $("#personal_last_name").val(),
            first_name: $("#personal_first_name").val(),
            pers_dob: $("#personal_birthdate_day").val() + "/" + $("#personal_birthdate_month").val() + "/" + $("#personal_birthdate_year").val(),
            phone_number: $("#personal_phone_number").val(),
            driverlicense: $("#driverlicense").val(),
            personal_number: $("#personal_number").val(),
            personal_street: $("#personal_street").val(),
            personal_state: $("#personal_state").val(),
            personal_city: $("#personal_city").val(),
            personal_postcode: $("#personal_postcode").val(),
            partnerToggle: partnerToggle
                    };
        $.extend(values, persValues);
        //if they select the partner selector i first append those input fields
        
        if (partnerToggle == "yes") {
            var partValues = {
                        partner_2_first_name: $("#partner_2_first_name").val(),
                        partner_2_last_name : $("#partner_2_last_name").val(),
                        partner_2_phone_number: $("#partner_2_phone_number").val(),
                        partner_2_dob: $("#partner_2_birthdate_day").val() + "/" + $("#partner_2_birthdate_month").val() + "/" + $("#partner_2_birthdate_year").val()
                };
            $.extend(values, partValues);
            var partnerAddr = $("input[name=shared2]:checked").val();
        if (partnerAddr == "no") {
            //and the selects separately so I will be able to compose the birthdate
            var partAddrValues = {partner_2_house_number: $("#partner_2_house_number").val(),
                        partner_2_street : $("#partner_2_street").val(),
                        partner_2_city: $("#partner_2_city").val(),
                partner_2_postcode: $("#partner_2_postcode").val(),
                partner_2_postcode: $("#partner_2_postcode").val(),
                partner_2_state: $("#partner_2_state").val(),
            };
            $.extend(values, partAddrValues);
        }
        }
        //finally I append the last fields
        var businessQ = {averageTransaction: $("#averageTransaction").val(),
            annualTurnover: $("#annualTurnover").val(),
            seasonal: $("input[name=seasonal]:checked").val(),
            locationUse: $("input[name=locationUse]:checked").val(),
            kungfuStyle: $("select[name=kungfuStyle]").val()
        };
            $.extend(values, businessQ);
        break;
    case 'soletrader':
        var values = {
            /*email: sessionStorage.getItem('user_mail'),
            merch_id: sessionStorage.getItem('user_id'),*/
            email: "abc@abc.com",
            merch_id: "arae",
            business_category: $("#business_category").val(),
            business_sub_category: $("#business_sub_category").val(),
            businessType: $("input[name=businessType]:checked").val(),
            business_abn_acn: $("input#business_abn_acn").val(),
            business_name: $("input#business_name").val(),
            last_name: $("#personal_last_name").val(),
            first_name: $("#personal_first_name").val(),
            pers_dob: $("#personal_birthdate_day").val() + "/" + $("#personal_birthdate_month").val() + "/" + $("#personal_birthdate_year").val(),
            phone_number: $("#personal_phone_number").val(),
            driverlicense: $("#driverlicense").val(),
            personal_number: $("#personal_number").val(),
            personal_street: $("#personal_street").val(),
            personal_state: $("#personal_state").val(),
            personal_city: $("#personal_city").val(),
            personal_postcode: $("#personal_postcode").val(),
            averageTransaction: $("#averageTransaction").val(),
            annualTurnover: $("#annualTurnover").val(),
            seasonal: $("input[name=seasonal]:checked").val(),
            locationUse: $("input[name=locationUse]:checked").val(),
            kungfuStyle: $("select[name=kungfuStyle]").val()
        };
        break;
    };
    /*
        var values = [];
        var i = 0;
        $('select').each(function () {
            // If input is visible and checked...
            if ($(this).is(':visible')) {
                array.push(this.name + ": " + $(this).val());
                i++;
            }
        });
        $('input').each(function () {
            // If input is visible and checked...
            if ($(this).is(':visible')) {
                array.push(this.name + ": " + $(this).val());
                i++;
            }
        });
    

    $("input[name^='partner_']").each(function () {
        // If input is visible and checked...
        if ($(this).is(':visible')) {
            console.log($(this).val());
        }
    });

    $("[name^='partner_2']").each(function () {
        // If input is visible and checked...
        if ($(this).is(':visible')) {
            console.log($(this).val());
        }
    });
    $("input[name^='partner_2']").each(function () {
                var NewData = '{'this.name': $(this).val()}';
                console.log(NewData);
                if ($(this).is(':visible')) {
                    values.push({'this.name': $(this).val()});
                }
            });*/

    //temporary solution for white spaces
$('#registration *').filter(':input').each(function (key) {
        if ($.trim($(key).val()) == "") {
            $(key).val("");
            console.log(key);
            return false;
        }
    });
if (document.getElementById('registration').checkValidity()) {
        add_data(values);
}
}

/*************************************

	add_data

*************************************/

function add_data(valuestoAdd) {
    start_blur();
//    setTimeout(function () {
        $.ajax({
            type: "post",
            data: valuestoAdd,
            dataType: "json",
            url: "../js/addAppCardData.php"
        }).done(function (valuestoAdd, status) {
            console.log(valuestoAdd.success);
            console.log(status);
            if (valuestoAdd.success) {
                console.log("Update success!");
                $("#commit").prop("disabled", true);
                //stop spinner
                stop_blur();
                //data insert was correct
                window.location.href = "Tappr_Phase3.html";
            } else {
                console.log("Update failure!");
                stop_blur();
                $.confirm({
                    text: "<b>Error:</b> " + valuestoAdd.errorLog,
                    title: "Error occurred during Insert",
                    confirm: function (button) {
                        //send the error via...
                    },
                    cancel: function (button) {},
                    confirmButton: "Send Error Report",
                    cancelButton: "No",
                    post: true
                });
                //data insert was incorrect
                //highlght the problem visually
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
            console.log(errorThrown);
            //trouble with the php
            stop_blur();
            $.confirm({
                text: "<b>Error:</b> " + textStatus + " "+jqXHR,
                title: "Error occurred during Processing",
                confirm: function (button) {},
                cancel: function (button) {},
                confirmButton: "Send Error Report",
                cancelButton: "No",
                post: true
            });
        });
  //  }, 5000);
}