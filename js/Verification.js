/*************************************

	is_valid

*************************************/

function is_valid(fld, tests){

	var val = $.trim($(fld).val());
	var field_type = arguments[1] ? arguments[1] : "text";
	var num_test = /^(?=.*\d.*\d).*$/;
	var symbol = /^(?=.*[!@#$%^&*]).*$/;
	//console.log(fld);
	var correct = true;
	var i, response = "", count = 0;
	//console.clear();

	for(i=0; i<tests.length; i++){

		count++;
		//console.log(typeof(tests[i]));

		if (typeof(tests[i]) == "number") {

			correct = val.length >= tests[i];

			if (!correct) {

				response = "Must be at least 8 characters long";

			}

		}else{

			switch(tests[i]){

			case "human":

				correct = false;
				if(val==12){
						correct = true;
				}

				if (!correct) {

					response = "Are you sure you're not a robot?";

				}

			break;

				case "empty":
					correct = val.length > 0;

					if (!correct) {

						response = "Value required";

					}
					break;

				case "abn":
					var test = $(fld).attr("data-test");
					var testCheck = $(fld).attr("data-test-check");
					var valid = $(fld).attr("data-valid");

					//console.log(test + ", " + testCheck + ", " + valid);

					if(testCheck != valid && $(fld).val() != ""){

						if(testCheck == ""  && valid == ""){

							correct = false;
							response = "Invalid ABN/ACN";

						}else{

							if(testCheck != valid){

								/*
								correct = $(this).val()  == test;

								if (!correct) {

									response = "Invalid ABN/ACN ya hear!";

								}else{


								}
								*/
									//console.log("hmmm");

								correct = false;
								response = test != "" ? "Validating" : "Invalid ABN/ACN";

							}else{

								correct = false;
								response = test != "" ? "Validating" : "Invalid ABN/ACN";

								//console.log("aha");

							}

						}
					}else{

						/*
						correct = $(this).val()  == valid;

						if (!correct) {

							response = "Invalid ABN/ACN";

						}
						*/

						correct = $(fld).attr("data-good") == "yes";

						if(!correct){
							response = $(fld).attr("data-response") != "" ? $(fld).attr("data-response") : "Invalid ABN/ACN";
						}
						//console.log("oh boy");
						//console.log(correct ? "ya ya" : "neigh");

					}
					break;

				case "email":
					correct = checkEmail(val);

					if (!correct.correct) {

						response = correct.response;

					}

					correct = correct.correct;
					break;

				case "numbers":
				   correct = num_test.test(val);

					if (!correct) {

						response = "Must contain at least 2 numbers";

					}
					break;


			}
		}

		if (!correct) {

			break;

		}
	}

	return {
		 correct: correct,
		 response : response
	};

}

/*************************************

	show_response

*************************************/


function show_response(tgt, type, valid, check, init){

	//console.log(tgt + " - " + ((!init || $.trim($("#" + tgt + " " + type).val()) != "") ? "show" : "hide"));

	if ((!check || check == tgt) && (!init || $.trim($("#" + tgt + " " + type).val()) != "")) {


		if (!valid.correct) {

			$("#" + tgt + " .validation-response").text(valid.response).fadeIn();

		}else{

			$("#" + tgt + " .validation-response").fadeOut(function(){$(this).empty()});

		}
	}

}

/*************************************

ABN lookup function

*************************************/



$("#business_abn_acn input[type=text]").on("blur", function(){
    console.log("lose focus");
	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			var weighting, i, len, check, acnVal = 8, lastDig, num, valid = {}, correct = false; tot = 0, abn = false, acn = false, val = $.trim($(this).val());
			var reg = new RegExp(/\d+/g);

			//console.log("val start = " + val);

			val = String(val.replace(/[^0-9]+/g, ''));

			//console.log("val numbers = " + val);

			if(val == null){

				//console.log("it's nothing");
				return;

			}else{

				len = val.length;

				//console.log("length = " + len);

				if(len == 9){

					// ACN

					//http://www.asic.gov.au/asic/asic.nsf/byheadline/Australian+Company+Number+%28ACN%29+Check+Digit#

					//console.log("acn");

					//weighting = [8,7,6,5,4,3,2,1];

					for(i=0; i<8; i++){

						check = Number(val.charAt(i));

						tot += check * acnVal;

						//console.log(check + " x " + acnVal + " = " + (check * acnVal));

						--acnVal;

					}

					//console.log("tot = " + tot );
					//console.log("tot % 10 = " + (tot % 10));

					tot = tot % 10;

					//console.log("10 - tot = " + (10 - tot));

					tot = 10 - tot;

					lastDig = Number(val.charAt(8));

					//console.log("lastDig = " + lastDig);

					//console.log(tot == lastDig ? "acn is fine" : "acn is bad");

					correct = lastDig == tot;

				}else if(len == 11){

					correct = true;

					/*

					It looks like this no longer applies as real abn dont always work

					https://www.ato.gov.au/Business/Australian-business-number/In-detail/Introduction/Format-of-the-ABN/

					// ABN

					console.log("abn");

					weighting = [10,1,3,5,7,9,11,13,15,17,19];

					for(i=0; i<val.length; i++){

						check = Number(val.charAt(i));

						if(i == 0){

							--check;

						}

						tot += check * weighting[i];

						console.log(check + " x " + weighting[i] + " = " + (check * weighting[i]));

					}

					console.log("tot = " + tot );
					console.log("tot / 89 = " + (tot / 89));

					tot = tot / 89;

					console.log("tot%5 = " + tot%5);

					console.log(tot%5 == 0 ? "abn is fine" : "abn is bad");

					correct = tot % 5 == 0;
					*/

				}else{

					//console.log("number count wrong");

				}
			}

			valid.correct = correct;

			if(valid.correct){

				//$(this).attr("data-test", $(this).val());
				//console.log("gut");

			}else{

				$(this).attr("data-test", "");
				$(this).attr("data-test-check", "");
				$(this).attr("data-valid", "");
				$(this).attr("data-good", "no");
				$(this).attr("data-response", "Invalid ABN/ACN");
				valid.response = "Invalid ABN/ACN";
				//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
				step3_proceed($(this).attr("name"));
				//console.log("nicht gut")
				return;

			}

			//if($(this).attr("data-test") != $(this).attr("data-valid") && !$(this).hasClass("validadting")){
			if(val != $(this).attr("data-valid") && $(this).val() != $(this).attr("data-test")){

				//$(this).addClass("validadting");
				$(this).attr("data-test", $(this).val());
				$(this).attr("data-test-check", val);

				//return;

				if(val != ""){

					var tgt = this;
					var testVal = $(this).val();
					step3_proceed($(tgt).attr("name"));

					$.ajax({
						type: "post",
						data: {
							abn: val
						},
						dataType: "json",
						url: "abn.php"
					}).done(function(data){

						//console.log(data);
						//console.log(data.error);

						//var html;
						//$(this).removeClass("validadting");

						if($(tgt).val() == testVal){

							$(tgt).attr("data-valid", "");

							if(data.success){

								correct = true;
								//console.log("ya = " + val + ".");



								if(confirm("Is this you?\n\n" + data.entityName)){

									$(tgt).attr("data-valid", val);
									//$(tgt).val(val);

									step3_proceed($(tgt).attr("name"));
									$(tgt).attr("data-good", "yes");

									$("#business_name input").val(data.entityName);
									$("#business_abn_acn input").val(data.abnacn);

								}else{

									$(tgt).attr("data-valid", "");
									$(tgt).attr("data-test", "");
									$(tgt).attr("data-test-check", "");
									$(tgt).attr("data-good", "no");
									$(tgt).attr("data-response", "");
									//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
									step3_proceed($(tgt).attr("name"));

								}

								step3_proceed($(tgt).attr("name"));

							}else{

								correct = false;
								response = data.reason;
								//console.log("na");

								valid.correct = false;
								valid.response = data.reason;
								$(tgt).attr("data-valid", "");
								$(tgt).attr("data-test", "");
								$(tgt).attr("data-test-check", "");
								$(tgt).attr("data-good", "no");
								$(tgt).attr("data-response", data.reason);
								//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
								step3_proceed($(tgt).attr("name"));

							}

						}

					}).fail(function(jqXHR, textStatus){

						if($(tgt).val() == testVal){

							correct = false;
							response = "could not connect";
							$(tgt).attr("data-valid", "");
							step3_proceed($(tgt).attr("name"));

						}

					});
				}

			}else{

				//console.log("hooey");

			}
			break;
	}


});


                            
/*************************************

	step 2 fields

*************************************/

//var retailVal = "";

$("#services select").on("change",function(){

	//step2_proceed($(this).attr("name"));

	var val = $(this).val(), i;
	//var oldVal = retailVal;

	//retailVal = $("#retail_type select").val();



	$("#retail_type select").empty();

	//console.log(val);

	if(val == ""){

		$("#retail_type select").append("<option value=''>Please choose a service first</option>");

	}else{

		$("#retail_type select").append("<option value=''>Please select</option>");

	}

	$.each(serviceList, function(key, value){

		if(key == val){

			//console.log("key = " + key + "\nval = " + val);

			$.each(value, function(index, nVal){

				$("#retail_type select").append("<option>" + nVal + "</option>");

			});

		}

	});

	//$("#retail_type select").val(oldVal);



});

/*************************************

	step2_proceed

*************************************/

function step2_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];

	//console.log("val = " + $("#services option:selected" ).val());


	valid = {correct: $( "#services option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("services", "option:selected", valid, check, init);

	valid = {correct: $( "#retail_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("retail_type", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

	return correct;

}

/*************************************

	step 3 fields

*************************************/

$("#businessUse input[type=text]").on("blur", function(e){

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			step3_proceed($(this).attr("name"));
			break;
	}


});



$("#businessUse input[type=text]").on("change",function(){

	step3_proceed($(this).attr("name"));

});

$("#businessUse select").on("change",function(){

	step3_proceed($(this).attr("name"));

});


/*************************************

	step3_proceed

*************************************/

function step3_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];

	valid = is_valid($("#business_name input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("business_name", "input", valid, check, init);

	valid = is_valid($("#business_abn_acn input"), ["empty", "abn"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("business_abn_acn", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("bus_type", "option:selected", valid, check, init);

	valid = {correct: $( "#annual_turnover option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("annual_turnover", "option:selected", valid, check, init);

	valid = {correct: $( "#employees option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("employees", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

    show_response("bus-address", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

    show_response("city", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

    show_response("post-code", "input", valid, check, init);

	valid = {correct: $( "#bus_type option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

    show_response("state", "option:selected", valid, check, init);

	valid = {correct: $( "#seasonal-bus option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("seasonal-bus", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}

	return correct;

}

/*************************************

	step 4 fields

*************************************/

$("#kungfu select").on("change",function(){

	step4_proceed($(this).attr("name"));

});

/*************************************

	step4_proceed

*************************************/

function step4_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];

	valid = {correct: $( "#kung_fu option:selected" ).val() != ""};

	if (!valid.correct) {

		correct = false;
		valid.response = "Please select";

	}

	show_response("kung_fu", "option:selected", valid, check, init);

	if(setFalse){

		correct = false;

	}


	return correct;

}

/*************************************

	step 5 fields

*************************************/

$("#about_you input[type=text]").on("blur", function(e){

	var code = e.which;

	switch(code){

		case 9:
		case 16:
			break;

		default:
			step5_proceed($(this).attr("name"));
			break;
	}


});

/*************************************

	step5_proceed

*************************************/

function step5_proceed(){

	var correct = true, valid;
	var check = arguments[0];
	var init = arguments[1];
	var setFalse = arguments[2];


	valid = is_valid($("#first_name input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("first_name", "input", valid, check, init);

	valid = is_valid($("#last_name input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("last_name", "input", valid, check, init);

	valid = is_valid($("#mobile input"), ["empty"]);

	if (!valid.correct) {

		correct = false;

	}

	show_response("mobile", "input", valid, check, init);

		valid = is_valid($("#human input"), ["empty"]);


	if (!valid.correct) {

		correct = false;

	}


		valid = is_valid($("#human input"), ["human"]);

		if (!valid.correct) {
		console.log(valid.response);
			correct = false;

		}

	show_response("human", "input", valid, check, init);



	if(setFalse){

		correct = false;

	}


	return correct;

}



/*************************************

	create_account();

*************************************/

function create_account() {

//	if($("#hdjksf-dsuf-ds8-90ad-sfd").val() != "789"){

		$("#got-to-create").hide();
		
		if(isiOS) {
			window.location = "juvo://clickedRegisterButton";
		} else {
			$("#loader").show();
		}

		var data={
			register: "yes",
			dCheck: $("#hdjksf-dsuf-ds8-90ad-sfd").val(),

			email: $("#email input").val(),
			password: $("#password input").val(),
			country: $("#country select").val(),

			services: $("#services select").val(),
			retail_type: $("#retail_type select").val(),

			business_name: $("#business_name input").val(),
			business_abn_acn: $("#business_abn_acn input").attr("data-valid"),
			bus_type: $("#bus_type select").val(),
			annual_turnover: $("#annual_turnover select").val(),
			employees: $("#employees select").val(),

			kung_fu: $("#kung_fu select").val(),

			first_name: $("#first_name input").val(),
			last_name: $("#last_name input").val(),
			dob: $("#dob_val").val(),
			mobile: $("#mobile input").val(),

			win_height: $(window).height(),
			win_width: $(window).width()

		}

		$.ajax({
			type: "post",
			data: data,
			dataType: "json",
			url: "connect.php"
		}).done(function(data){

			//console.log(data);
			//console.log(data.error);

			//var html;

			if(data.success){

				onRegistrationSuccessForiOS(data);

				$("#got-to-juvo, #pre-order-juvo").removeAttr("disabled");
				$("#send-data-start").hide();
				$("#send-data-bad").hide();
				$("#send-data-good").show();

				//html = "good - " + data.result;
				$("#juvo-link").val(data.result);

			}else{

				onRegistrationFailureForiOS();
				
				$("#try-again").removeAttr("disabled");
				$("#got-to-juvo, #pre-order-juvo").attr("disabled","disabled");
				$("#fail-reason").html(data.error);
				//html = "bad: " + data.error;
				$("#send-data-start").hide();
				$("#send-data-good").hide();
				$("#send-data-bad").show();

			}

			//$("#test-data").html(html);

		}).fail(function(jqXHR, textStatus){

			//$("#test-data").html("Unable to save settings: " + textStatus + "<br>" + data);
			
			onRegistrationFailureForiOS();

			$("#try-again").removeAttr("disabled");
			$("#got-to-juvo, #pre-order-juvo").attr("disabled","disabled");
			$("#fail-reason").html(textStatus);
			//$("#send-data-bad .start-text").html("Unable to save settings: " + textStatus);
			$("#send-data-start").hide();
			$("#send-data-good").hide();
			$("#send-data-bad").show();

		});
//	}

}

/*************************************

	removeA

*************************************/

function removeA(arr) {

	var what, a = arguments, L = a.length, ax;

	while (L > 1 && arr.length) {

		what = a[--L];

		while ((ax= arr.indexOf(what)) !== -1) {

			arr.splice(ax, 1);

		}
	}

	return arr;

}

/*************************************

	winTall

*************************************/

function winTall(){

	var tall = $(window).height() < minScreenTall.height;
	var wide = $(window).width() < minScreenTall.width;

	return $(window).height() < minScreenTall.height && $(window).width() < minScreenTall.width;


}

/*************************************

	winWide

*************************************/

function winWide(){

	var tall = $(window).height() < minScreenWide.height;
	var wide = $(window).width() < minScreenWide.width;

	return $(window).height() < minScreenWide.height && $(window).width() < minScreenWide.width;


}

/*************************************

	inBox

*************************************/

function inBox(){

	if(winWide()){

		return true;

	}

	if(winTall()){

		return true;

	}

	return false;


}

winHeight = $(window).height();
winWidth = $(window).width();
var minHeight = 400;
var minScreenTall = {height: 650, width: 470};
var minScreenWide = {height: 470, width: 650};

/*************************************

	ready

*************************************/



$().ready(function(){

	//console.log(winHeight + ", " + winWidth);

/*Future implementation */
	/*
	var d = new Date();

    var yearNow = d.getFullYear() - 18;
	var yearStart = yearNow - 80;

	$( "#dob input" ).datepicker({
		dateFormat: "d MM yy",
		defaultDateType: 0,
		yearRange: yearStart+ ":" + yearNow,
		changeMonth: true,
		changeYear: true,
		altField: "#dob_val",
		altFormat: "yy-mm-dd",
		maxDate: "-18y"
	});*/

	//$( "#dob input" ).datepicker("setDate"

	$("section").blur(function(){

		var home = $("*:focus").closest("section");

		//console.log("i am in " + home.attr("id"));
		//console.log($("*:focus").closest("section").offset());

		if(home.attr("id") !== undefined){

			var tgtId = $("*:focus").closest("section").attr("id");
			var curId = currentSection.attr("id");


		}
	});

	$(document).on("keypup", "section", function(e){

	});


	$(".next-btn").on("click",function(){

		if (!$(this).hasClass("inactive")) {

			var next_section = $(this).closest("section").next("section");

		}
	});

    $(".next-btn-bus").on("click",function(){

		if (!$(this).hasClass("inactive")) {

			var next_section = $(this).closest("section").next("section");

		}
	});

});


var serviceList = {
	"Beauty & Personal Care": [
		"Beauty Salon",
		"Hair Salon/ Barbershop",
		"Independent Stylist/ Barber",
		"Massage Therapist",
		"Nail Salon",
		"Spa",
		"Tanning Salon",
		"Tattoo/ Piercing",
		"Other"
	],
	"Charity": [
		"Charitable Organisation",
		"Child Care",
		"Instructor/ Teacher",
		"Membership Organisation",
		"Political Organisation",
		"Religious Organisation",
		"School",
		"Tutor",
		"Other"
	],
	"Food & Drink": [
		"Bakery",
		"Bar/ Club/ Lounge",
		"Caterer",
		"Coffee/ Tea Store",
		"Convenience Store",
		"Food Truck/ Cart",
		"Grocery/ Market",
		"Outdoor Market",
		"Private Chef",
		"Quick Service Restaurant",
		"Sit Down Restaurant",
		"Speciality Store",
		"Other"
	],
	"Health Care": [
		"Acupuncture",
		"Alternative Medicines",
		"Carer",
		"Chiropractor",
		"Dentist/ Orthodontist",
		"Gym",
		"Massage Therapist",
		"Medical Practitioner",
		"Optometrist/ Eye Care",
		"Personal Trainer",
		"Psychiatrist",
		"Therapist",
		"Veterinary Services",
		"Other"
	],
	"Home & Repair": [
		"Automotive Parts, Repair",
		"Carpentry Services",
		"Carpet Cleaning",
		"Cleaning",
		"Clothing Repair/ Shoe Repair",
		"Computer/ Electronics Repair",
		"Dry Cleaning",
		"Electrical Services",
		"Flooring",
		"General Contracting",
		"Heating & Air-Con",
		"Installation Services",
		"Junk Removal",
		"Landscaping",
		"Locksmiths Services",
		"Masonry, Stonework",
		"Moving/ Removalist",
		"Painting",
		"Pest Control",
		"Plumbing",
		"Roofing",
		"Watch/ Jewellery Services",
		"Other"
	],
	"Leisure":[
		"Events/ Festivals",
		"Movies/ Film",
		"Museum/ Cultural",
		"Music",
		"Performing Arts",
		"Sporting Events",
		"Sport Recreation",
		"Tourism",
		"Other"
	],
	"Professional Services": [
		"Accounting",
		"Child Care",
		"Consulting",
		"Delivery",
		"Design",
		"Interior Design",
		"Legal Services",
		"Marketing/ Advertising",
		"Nanny Services",
		"Photography",
		"Printing",
		"Real Estate",
		"Software Development",
		"Other"
	],
	"Retail":[
		"Art, Photo, Film",
		"Books, Magazines, Music, Video",
		"Clothing and Accessories",
		"Convenience Store",
		"Electronics Store",
		"Eye Wear Store",
		"Flowers & Gifts",
		"Furniture",
		"Grocery/ Markets",
		"Hardware Store",
		"Hobby Shop",
		"Jewellery Store",
		"Office Supply",
		"Outdoor Markets",
		"Pet Store",
		"Publishing/ Printing",
		"Speciality Store",
		"Sporting Goods",
		"Other"
	],
	"Transportation":[
		"Bus Services",
		"Delivery",
		"Limousine",
		"Moving",
		"Private Shuttle",
		"Taxi Services",
		"Town Car Services",
		"Other"
	],
	"Causal Use":[
		"Events",
		"Miscellaneous Goods",
		"Miscellaneous Services",
		"Outdoor Markets/ Stands",
		"Other"
	]
};

// possible native mobile select function

/*
*/
