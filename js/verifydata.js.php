/*
<?php
session_start();
$session_name = "juvoRegister_";
?>
*/ 


<!--when the main category is selected, populate the submenu-->
$('#business_category').on('change', function () {	
        $('#business_sub_category').populate( serviceList[this.value], {
            onPopulate: function (opts) {
                // Handle empty results
                if ( !opts.length ) {
                    $(this).attr('disabled', 'disabled').append('<option>No subcategories available</option>');
                }
            }
        });
});


$("input").change(function() {
    if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
        $(this).css("border", "3px solid #f00");
    } else {
        $(this).css("border", "1px solid #dfdfdf");
    }
});

$("select").change(function() {
    if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
        $(this).css("border", "3px solid #f00");
    } else {
        $(this).css("border", "1px solid #dfdfdf");
    }
});

/*$("#commit").on("click",function(){
    if (Boolean($(this)[0].checkValidity) && (! $(this)[0].checkValidity())) {
        $(this).css("border", "3px solid #f00");
    } else {
        $(this).css("border", "1px solid #dfdfdf");
    }
});*/

<!--when email loses focus check the consistency--> 
$("input#business_abn_acn").on("blur",function(){
            console.clear();
			var weighting, i, len, check, acnVal = 8, lastDig, num, valid = {}, correct = false; tot = 0, abn = false, acn = false, val = $.trim($(this).val());
			var reg = new RegExp(/\d+/g);
			//console.log("val start = " + val);
			val = String(val.replace(/[^0-9]+/g, ''));
			//console.log("val numbers = " + val);
			if(val == null){
				//console.log("it's nothing");
				return;
			}else{
				len = val.length;
				//console.log("length = " + len);
				if(len == 9){
					// ACN
					//http://www.asic.gov.au/asic/asic.nsf/byheadline/Australian+Company+Number+%28ACN%29+Check+Digit#
					//console.log("acn");
					//weighting = [8,7,6,5,4,3,2,1];
					for(i=0; i<8; i++) {
						check = Number(val.charAt(i));
						tot += check * acnVal;
						//console.log(check + " x " + acnVal + " = " + (check * acnVal));
						--acnVal;
					}
					//console.log("tot = " + tot );
					//console.log("tot % 10 = " + (tot % 10));
					tot = tot % 10;
					//console.log("10 - tot = " + (10 - tot));
					tot = 10 - tot;
					lastDig = Number(val.charAt(8));
					//console.log("lastDig = " + lastDig);
					//console.log(tot == lastDig ? "acn is fine" : "acn is bad");
					correct = lastDig == tot;
				}else if(len == 11){
					correct = true;
				}else{
					console.log("number count wrong");
				}
			}
			valid.correct = correct;
			if(!valid.correct){
				$(this).attr("data-test", "");
				$(this).attr("data-test-check", "");
				$(this).attr("data-valid", "");
				$(this).attr("data-good", "no");
				$(this).attr("data-response", "Invalid ABN/ACN");
				valid.response = "Invalid ABN/ACN";
				//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
				//step3_proceed($(this).attr("name"));
				//console.log("nicht gut")
				return;
			}

			//if($(this).attr("data-test") != $(this).attr("data-valid") && !$(this).hasClass("validadting")){
			if(val != $(this).attr("data-valid") && $(this).val() != $(this).attr("data-test")) {
				//$(this).addClass("validadting");
				$(this).attr("data-test", $(this).val());
				$(this).attr("data-test-check", val);
				//return;
				if(val != ""){
					var tgt = this;
					var testVal = $(this).val();
					//step3_proceed($(tgt).attr("name"));
					$.ajax({
						type: "post",
						data: {
							abn: val
						},
						dataType: "json",
						url: "../js/abn.php"
					}).done(function(data){
						//console.log(data);
						if($(tgt).val() == testVal){
							$(tgt).attr("data-valid", "");
							if(data.success){
								correct = true;
								$.confirm({
                                    text:  "<b>"+data.entityName + "</b>, is this you?",
                                    title: "Confirmation required",
                                    confirm: function(button) {
                                        //console.log(data.entityName);
                                        $(tgt).prop("data-valid", val);
                                        $(tgt).prop("data-good", "yes");
                                        $("input#business_name").val(data.entityName);
                                        $("input#business_abn_acn").val(data.abnacn);
                                    },
                                    cancel: function(button) {
                                        $(tgt).attr("data-valid", "");
                                        $(tgt).attr("data-test", "");
                                        $(tgt).attr("data-test-check", "");
                                        $(tgt).attr("data-good", "no");
                                        $(tgt).attr("data-response", "");
                                        //show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
                                        //step3_proceed($(tgt).attr("name"));
                                    },
                                    confirmButton: "Yes",
                                    cancelButton: "No",
                                    post: true
                                });
								//step3_proceed($(tgt).attr("name"));
							}else{
								correct = false;
								response = data.reason;
								//console.log("na");
								valid.correct = false;
								valid.response = data.reason;
								$(tgt).attr("data-valid", "");
								$(tgt).attr("data-test", "");
								$(tgt).attr("data-test-check", "");
								$(tgt).attr("data-good", "no");
								$(tgt).attr("data-response", data.reason);
								//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
								//step3_proceed($(tgt).attr("name"));
							}
						}
					}).fail(function(jqXHR, textStatus){
						if($(tgt).val() == testVal){
							correct = false;
							response = "could not connect";
							$(tgt).attr("data-valid", "");
							//step3_proceed($(tgt).attr("name"));
						}
					});
				}
			}else{
				console.log("hooey");
			}
});
                                  
/*************************************

	show_response

*************************************/


function show_response(tgt, type, valid, check, init){
	//console.log(tgt + " - " + ((!init || $.trim($("#" + tgt + " " + type).val()) != "") ? "show" : "hide"));
	if ((!check || check == tgt) && (!init || $.trim($("#" + tgt + " " + type).val()) != "")) {
		if (!valid.correct) {
			$("span#" + tgt + ".validation-response").text(valid.response).fadeIn();
		}else{
			$("span#" + tgt + ".validation-response").fadeOut(function(){$(this).empty()});
		}
	}
}

/*************************************

	is_valid

*************************************/

function is_valid(fld, tests){

	var val = $.trim($(fld).val());
	var field_type = arguments[1] ? arguments[1] : "text";
	var num_test = /^(?=.*\d.*\d).*$/;
	var symbol = /^(?=.*[!@#$%^&*]).*$/;
	console.log(fld);
	var correct = true;
	var i, response = "", count = 0;
	//console.clear();

	for(i=0; i<tests.length; i++){
		count++;
		//console.log(typeof(tests[i]));
		if (typeof(tests[i]) == "number") {
			correct = val.length >= tests[i];
			if (!correct) {
				response = "Must be at least 8 characters long";
			}
		}else{
			switch(tests[i]){
				case "empty":
					correct = val.length > 0;
					if (!correct) {
						response = "Value required";
					}
				break;
				case "abn":
					var test = $(fld).attr("data-test");
					var testCheck = $(fld).attr("data-test-check");
					var valid = $(fld).attr("data-valid");
					//console.log(test + ", " + testCheck + ", " + valid);
					if(testCheck != valid && $(fld).val() != ""){
						if(testCheck == ""  && valid == ""){
							correct = false;
							response = "Invalid ABN/ACN";
						}else{
							if(testCheck != valid){
								/*
								correct = $(this).val()  == test;
								if (!correct) {
									response = "Invalid ABN/ACN ya hear!";
								}else{
								}
								*/
									//console.log("hmmm");
								correct = false;
								response = test != "" ? "Validating" : "Invalid ABN/ACN";
							}else{
								correct = false;
								response = test != "" ? "Validating" : "Invalid ABN/ACN";
								//console.log("aha");
							}
						}
					}else{
						/*
						correct = $(this).val()  == valid;
						if (!correct) {
							response = "Invalid ABN/ACN";
						}
						*/
						correct = $(fld).attr("data-good") == "yes";
						if(!correct){
							response = $(fld).attr("data-response") != "" ? $(fld).attr("data-response") : "Invalid ABN/ACN";
						}
						//console.log("oh boy");
						//console.log(correct ? "ya ya" : "neigh");
					}
				break;
				case "numbers":
				   correct = num_test.test(val);
					if (!correct) {
						response = "Must contain at least 2 numbers";
					}
					break;
			}
		}
		if (!correct) {
			break;
		}
	}
	return {
		 correct: correct,
		 response : response
	};

}

var serviceList = {
	"beauty_and_personal_care": {
	"beauty_salon" : "Beauty Salon",
"Hair Salon / Barbershop" : "Hair Salon / Barbershop",
"Independent Stylist / Barber" : "Independent Stylist / Barber",
"Massage Therapist" : "Massage Therapist",
"Nail Salon" : "Nail Salon",
"Spa" : "Spa",
"Tanning Salon" : "Tanning Salon",
"Tattoo / Piercing" : "Tattoo / Piercing",
	"other" : "Other"
	},
	"charities_education_and_membership": {
"Charitable Organisation" : "Charitable Organisation",
"Child Care" : "Child Care",
"Instructor / Teacher" : "Instructor / Teacher",
"Membership Organisation" : "Membership Organisation",
"Political Organisation" : "Political Organisation",
"Religious Organisation" : "Religious Organisation",
"School" : "School",
"Tutor" : "Tutor",
	"other" : "Other"
	},
	"food_and_drink": {
"Bakery" : "Bakery",
"Bar / Club / Lounge" : "Bar / Club / Lounge",
"Caterer" : "Caterer",
"Coffee / Tea Store" : "Coffee / Tea Store",
"Convenience Store" : "Convenience Store",
"Food Truck / Cart" : "Food Truck / Cart",
"Grocery / Market" : "Grocery / Market",
"Outdoor Market" : "Outdoor Market",
"Private Chef" : "Private Chef",
"Quick Service Restaurant" : "Quick Service Restaurant",
"Sit Down Restaurant" : "Sit Down Restaurant",
"Speciality Store" : "Speciality Store",
	"other" : "Other"
	},
	"health_care_and_fitness": {
"Acupuncture" : "Acupuncture",
"Alternative Medicines" : "Alternative Medicines",
"Carer" : "Carer",
"Chiropractor" : "Chiropractor",
"Dentist / Orthodontist" : "Dentist / Orthodontist",
"Gym" : "Gym",
"Massage Therapist" : "Massage Therapist",
"Medical Practitioner" : "Medical Practitioner",
"Optometrist / Eye Care" : "Optometrist / Eye Care",
"Personal Trainer" : "Personal Trainer",
"Psychiatrist" : "Psychiatrist",
"Therapist" : "Therapist",
"Veterinary Services" : "Veterinary Services",
	"other" : "Other"
	},
	"home_and_repair": {
"Automotive Parts, Repair" : "Automotive Parts, Repair",
"Carpentry Services" : "Carpentry Services",
"Carpet Cleaning" : "Carpet Cleaning",
"Cleaning" : "Cleaning",
"Clothing Repair / Shoe Repair" : "Clothing Repair / Shoe Repair",
"Computer / Electronics Repair" : "Computer / Electronics Repair",
"Dry Cleaning" : "Dry Cleaning",
"Electrical Services" : "Electrical Services",
"Flooring" : "Flooring",
"General Contracting" : "General Contracting",
"Heating & Air-Con" : "Heating & Air-Con",
"Installation Services" : "Installation Services",
"Junk Removal" : "Junk Removal",
"Landscaping" : "Landscaping",
"Locksmiths Services" : "Locksmiths Services",
"Masonry, Stonework" : "Masonry, Stonework",
"Moving / Removalist" : "Moving / Removalist",
"Painting" : "Painting",
"Pest Control" : "Pest Control",
"Plumbing" : "Plumbing",
"Roofing" : "Roofing",
"Watch / Jewellery Services" : "Watch / Jewellery Services",
	"other" : "Other"
	},
	"leisure_and_entertainment": {
"Events / Festivals" : "Events / Festivals",
"Movies / Film" : "Movies / Film",
"Museum / Cultural" : "Museum / Cultural",
"Music" : "Music",
"Performing Arts" : "Performing Arts",
"Sporting Events" : "Sporting Events",
"Sport Recreation" : "Sport Recreation",
"Tourism" : "Tourism",
	"other" : "Other"
	},
	"professional_services": {
"Accounting" : "Accounting",
"Child Care" : "Child Care",
"Consulting" : "Consulting",
"Delivery" : "Delivery",
"Design" : "Design",
"Interior Design" : "Interior Design",
"Legal Services" : "Legal Services",
"Marketing / Advertising" : "Marketing / Advertising",
"Nanny Services" : "Nanny Services",
"Photography" : "Photography",
"Printing" : "Printing",
"Real Estate" : "Real Estate",
"Software Development" : "Software Development",
	"other" : "Other"
	},
	"retail": {
"Art, Photo, Film" : "Art, Photo, Film",
"Books, Magazines, Music, Video" : "Books, Magazines, Music, Video",
"Clothing and Accessories" : "Clothing and Accessories",
"Convenience Store" : "Convenience Store",
"Electronics Store" : "Electronics Store",
"Eye Wear Store" : "Eye Wear Store",
"Flowers & Gifts" : "Flowers & Gifts",
"Furniture" : "Furniture",
"Grocery / Markets" : "Grocery / Markets",
"Hardware Store" : "Hardware Store",
"Hobby Shop" : "Hobby Shop",
"Jewellery Store" : "Jewellery Store",
"Office Supply" : "Office Supply",
"Outdoor Markets" : "Outdoor Markets",
"Pet Store" : "Pet Store",
"Publishing / Printing" : "Publishing / Printing",
"Speciality Store" : "Speciality Store",
"Sporting Goods" : "Sporting Goods",
	"other" : "Other"
	},
	"transportation": {
"Bus Services" : "Bus Services",
"Delivery" : "Delivery",
"Limousine" : "Limousine",
"Moving" : "Moving",
"Private Shuttle" : "Private Shuttle",
"Taxi Services" : "Taxi Services",
"Town Car Services" : "Town Car Services",
	"other" : "Other"
	},
	"casual_use": {
"Events" : "Events",
"Miscellaneous Goods" : "Miscellaneous Goods",
"Miscellaneous Services" : "Miscellaneous Services",
"Outdoor Markets / Stands" : "Outdoor Markets / Stands",
	"other" : "Other"
    }
};