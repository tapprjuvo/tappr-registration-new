<?php
session_start();
$session_name = "juvoRegister_";

header('Content-type: application/json');

$return['error'] = true;

function clearData($val, $quotes = true) {
    $val = addslashes(trim($val));
    $val = str_replace("&", "", $val);
    
    return $quotes ? "'$val'" : $val;
}

    $conn = mysqli_connect("192.168.0.33", "root", "sysdba", "tappr_reg");
    
    // Check connection
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
    if ($conn) {
        $sql = "UPDATE merchant_data SET " . 
          "business_services=" . clearData($_REQUEST["business_category"]) . ", " .
            "business_sub_services=" . clearData($_REQUEST["business_sub_category"]) . ", " .
            "business_type=" . clearData($_REQUEST["businessType"]) . ", " .
            "business_name=" . clearData($_REQUEST["business_name"]) . ", " .
            "business_abn_acn=" . clearData($_REQUEST["business_abn_acn"]) . ", " .
            "first_name=" . clearData($_REQUEST["first_name"]) . ", " .
            "last_name=" . clearData($_REQUEST["last_name"]) . ", " .
            "phone_number=" . clearData($_REQUEST["phone_number"]) . ", " .
            "user_agent=" . clearData($_SERVER["HTTP_USER_AGENT"]) . ", " .
            "ip_address=" . clearData($_SERVER["REMOTE_ADDR"]) . ", " .
            "personal_street=" . clearData($_REQUEST["address_street"]) . ", " .
            "personal_house_number=" . clearData($_REQUEST["address_number"]) . ", " .
            "personal_postcode=" . clearData($_REQUEST["postal_code"]) . ", " .
            "personal_city=" . clearData($_REQUEST["address_city"]) . ", " .
            "personal_state=" . clearData($_REQUEST["address_state"]) . ", " .
            "average_transaction=" . clearData($_REQUEST["averageTransaction"]) . ", " .
            "annual_turnover=" . clearData($_REQUEST["annualTurnover"]) . ", " .
            "seasonal=" . clearData($_REQUEST["seasonal"]) . ", " .
            "location_type=" . clearData($_REQUEST["locationUse"]) . ", " .
            "kung_fu=" . clearData($_REQUEST["kungfuStyle"]) . " " .
          "WHERE merchant_id =" . clearData($_REQUEST["merch_id"]) . " " .
          "AND email =" . clearData($_REQUEST["email"]) . "";
        
        $insert = mysqli_query($conn, $sql);
        
        if ($insert) {
            $return['error'] = false;
        } else {
            echo "Error: " . mysqli_error($conn);
        }
        mysqli_close($conn);
    } else {
        echo "Could not connect to the DB: " . mysqli_error($conn); //redundant?
        exit;
    }

echo json_encode($return);

?>