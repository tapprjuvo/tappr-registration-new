<div>
<input type="text" name="field[ABN]" placeholder="abn/acn" class="abnacn_field" />
<input type="text" name="field[BUSINESS]" placeholder="Trading name" class="trading_name_field" />
  <input type="submit" class="abnacn" name="submit" value="Validate ABN/ACN Details" />
<img src="loader.gif" style="display:none" class="loader" />
<input type="submit" name="submit" value="Next" class="abn_proceed" disabled="disabled" />
</div>

<div>
<form method="post" action="validate.php">
<input type="text" name="field[EMAIL]" class="email" placeholder="Email" />
<input type="submit" name="submit" value="Next" class="email_proceed" disabled="disabled" />
</form>
</div>

<div>
<form method="post" action="validate.php">
<input type="text" name="field[PASSWORD]" placeholder="password" class="password" />
<div class="box password_valid_number"></div>
<div class="box password_valid_length"></div>
<div class="box password_valid_symbol"></div>
<div class="box password_valid"></div>
<input type="submit" name="submit" class="password_proceed" value="Next" disabled="disabled" />
</form>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

	$(".abnacn").on("click",function(){
		$(".loader").show();
		$.post("validate.php",{"field[ABN]":$(".abnacn_field").val(),"field[BUSINESS]":$(".trading_name_field").val() },function(data){
			if( data=="true" ){
				$(".abn_proceed").removeAttr("disabled");
			}else{
				$(".abn_proceed").attr("disabled","disabled");
			}
			$(".loader").hide();
		});
	});

	$(".email").on("keyup",function(){
		$.post("validate.php",{"field[EMAIL]":$(".email").val()},function(data){
			if(data=="true"){
				$(".email_proceed").removeAttr("disabled");
			}else{
				$(".email_proceed").attr("disabled","disabled");
			}
		});
	});

	$(".password").on("keyup",function(){
		$.post("validate.php",{"field[PNUMBER]":$(".password").val()},function(data){
			if(data=="true"){
				$(".password_valid_number").addClass("ok");
				$(".password_valid_number").text("OK");
			}else{
				$(".password_valid_number").removeClass("ok");
				$(".password_valid_number").text("Password must contain at least 2 numbers");
			}
		});
		$.post("validate.php",{"field[PSYMBOL]":$(".password").val()},function(data){
                        if(data=="true"){
                                $(".password_valid_symbol").addClass("ok");
                                $(".password_valid_symbol").text("OK");
                        }else{
                                $(".password_valid_symbol").removeClass("ok");
                                $(".password_valid_symbol").text("Password must contain at least 1 symbol like ! @ # $ % ^ & *");
                        }
                });
                
                
		$.post("validate.php",{"field[PLENGTH]":$(".password").val()},function(data){
                        if(data=="true"){
                                $(".password_valid_length").addClass("ok");
                                $(".password_valid_length").text("OK");
                        }else{
                                $(".password_valid_length").removeClass("ok");
                                $(".password_valid_length").text("Password must be at least 8 characters long");
                        }
                });
	
	        $.post("validate.php",{"field[PASSWORD]":$(".password").val()},function(data){
                      
                        if(data=="true"){
                              $(".password_valid").addClass("ok");
                                $(".password_valid").text("Your password meets our security criteria");
				$(".password_proceed").removeAttr("disabled");
                        }else{
                              $(".password_valid").text("Your password must meet our security criteria");
                                $(".password_valid").removeClass("ok");
				$(".password_proceed").attr("disabled","disabled");
                        }
                });	

	});

});

</script>

<style type="text/css">
.box{
width:400px;
height:20px;
margin:2px;
background:#ff0000;
}
.ok{
background:#31B32D;
}
</style>
