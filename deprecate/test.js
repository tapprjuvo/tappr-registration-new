			console.clear();
			var weighting, i, len, check, acnVal = 8, lastDig, num, valid = {}, correct = false; tot = 0, abn = false, acn = false, val = $.trim($(this).val());
			var reg = new RegExp(/\d+/g);
			//console.log("val start = " + val);
			val = String(val.replace(/[^0-9]+/g, ''));
			//console.log("val numbers = " + val);
			if(val == null){
				//console.log("it's nothing");
				return;
			}else{
				len = val.length;
				//console.log("length = " + len);
				if(len == 9){
					// ACN
					//http://www.asic.gov.au/asic/asic.nsf/byheadline/Australian+Company+Number+%28ACN%29+Check+Digit#
					//console.log("acn");
					//weighting = [8,7,6,5,4,3,2,1];
					for(i=0; i<8; i++){
						check = Number(val.charAt(i));
						tot += check * acnVal;
						//console.log(check + " x " + acnVal + " = " + (check * acnVal));
						--acnVal;
					}
					//console.log("tot = " + tot );
					//console.log("tot % 10 = " + (tot % 10));
					tot = tot % 10;
					//console.log("10 - tot = " + (10 - tot));
					tot = 10 - tot;
					lastDig = Number(val.charAt(8));
					//console.log("lastDig = " + lastDig);
					//console.log(tot == lastDig ? "acn is fine" : "acn is bad");
					correct = lastDig == tot;
				}else if(len == 11){
					correct = true;
					/*It looks like this no longer applies as real abn dont always work
                    https://www.ato.gov.au/Business/Australian-business-number/In-detail/Introduction/Format-of-the-ABN/

					// ABN
					console.log("abn");
					weighting = [10,1,3,5,7,9,11,13,15,17,19];
					for(i=0; i<val.length; i++){
						check = Number(val.charAt(i));
						if(i == 0){
							--check;
						}
						tot += check * weighting[i];
						console.log(check + " x " + weighting[i] + " = " + (check * weighting[i]));
					}
					console.log("tot = " + tot );
					console.log("tot / 89 = " + (tot / 89));
					tot = tot / 89;
					console.log("tot%5 = " + tot%5);
					console.log(tot%5 == 0 ? "abn is fine" : "abn is bad");
					correct = tot % 5 == 0;
					*/
				}else{
					console.log("number count wrong");
				}
			}
			valid.correct = correct;
			if(valid.correct){
				//$(this).attr("data-test", $(this).val());
				console.log("gut");
			}else{
				$(this).attr("data-test", "");
				$(this).attr("data-test-check", "");
				$(this).attr("data-valid", "");
				$(this).attr("data-good", "no");
				$(this).attr("data-response", "Invalid ABN/ACN");
				valid.response = "Invalid ABN/ACN";
				//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
				//step3_proceed($(this).attr("name"));
				//console.log("nicht gut")
				return;
			}

			//if($(this).attr("data-test") != $(this).attr("data-valid") && !$(this).hasClass("validadting")){
			if(val != $(this).attr("data-valid") && $(this).val() != $(this).attr("data-test")) {
				//$(this).addClass("validadting");
				$(this).attr("data-test", $(this).val());
				$(this).attr("data-test-check", val);
				//return;
				if(val != ""){
					var tgt = this;
					var testVal = $(this).val();
					//step3_proceed($(tgt).attr("name"));
					$.ajax({
						type: "post",
						data: {
							abn: val
						},
						dataType: "json",
						url: "../js/abn.php"
					}).done(function(data){
						//console.log(data);
						//console.log(data.error);
						//var html;
						//$(this).removeClass("validadting");
						if($(tgt).val() == testVal){
							$(tgt).attr("data-valid", "");
							if(data.success){
								correct = true;
								//console.log("ya = " + val + ".");
								if(confirm("Is this you?\n\n" + data.entityName)){
									$(tgt).prop("data-valid", val);
									//$(tgt).val(val);
									//step3_proceed($(tgt).attr("name"));
									$(tgt).prop("data-good", "yes");
									$("input##business_name").val(data.entityName);
									$("input#business_abn_acn").val(data.abnacn);
								}else{
									$(tgt).attr("data-valid", "");
									$(tgt).attr("data-test", "");
									$(tgt).attr("data-test-check", "");
									$(tgt).attr("data-good", "no");
									$(tgt).attr("data-response", "");
									//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
									//step3_proceed($(tgt).attr("name"));
								}
								//step3_proceed($(tgt).attr("name"));
							}else{
								correct = false;
								response = data.reason;
								//console.log("na");
								valid.correct = false;
								valid.response = data.reason;
								$(tgt).attr("data-valid", "");
								$(tgt).attr("data-test", "");
								$(tgt).attr("data-test-check", "");
								$(tgt).attr("data-good", "no");
								$(tgt).attr("data-response", data.reason);
								//show_response("business_abn_acn", "input", valid, "business_abn_acn", false);
								//step3_proceed($(tgt).attr("name"));
							}
						}
					}).fail(function(jqXHR, textStatus){
						if($(tgt).val() == testVal){
							correct = false;
							response = "could not connect";
							$(tgt).attr("data-valid", "");
							//step3_proceed($(tgt).attr("name"));
						}
					});
				}
			}else{
				console.log("hooey");
			}