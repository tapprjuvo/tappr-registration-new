//tealium universal tag - utag.12 ut4.0.201403261920, Copyright 2014 Tealium.com Inc. All Rights Reserved.
try{(function(id,loader,u){try{u=utag.o[loader].sender[id]={}}catch(e){u=utag.sender[id]};u.ev={'view':1,'link':1};u.qsp_delim="&";u.kvp_delim="=";u.appid="46270";u.name="landing";u.qty=[];u.value=[];u.sku=[];u.base_url="//api.nanigans.com/event.php?";u.map={"_nanigan_type":"type","_nanigan_name":"name"};u.extend=[function(a,b){if(1){b['_nanigan_name']='landing';b['_nanigan_type']='visit'}}];u.send=function(a,b){if(u.ev[a]||typeof u.ev.all!="undefined"){for(c=0;c<u.extend.length;c++){try{d=u.extend[c](a,b);if(d==false)return}catch(e){}};var c,d,e,f,g;c=[];g=[];for(d in utag.loader.GV(u.map)){if(typeof b[d]!="undefined"&&b[d]!=""){e=u.map[d].split(",");for(f=0;f<e.length;f++){u[e[f]]=b[d];}}}
if(u.name=="landing"){u.type=u.type||"visit";}
if(u.name=="reg"||u.name=="skin"){u.type=u.type||"install";}
if(u.name=="login"){u.type=u.type||"user";}
if(u.name=="add_to_cart"||b._corder){if(!(u.qty instanceof Array)){u.qty=u.qty.split(',');}
if(!(u.value instanceof Array)){u.value=u.value.split(',');}
if(!(u.sku instanceof Array)){u.sku=u.sku.split(',');}}
if(u.name=="add_to_cart"){u.type=u.type||"user";u.qty=(u.qty.length>0?u.qty[0]:b._cquan[0]);u.value=(u.value.length>0?u.value[0]:b._cprice[0]);u.value=u.value.replace(".","");u.sku=(u.sku.length>0?u.sku[0]:b._cprod[0]);g.push("qty="+u.qty,"value="+u.value,"sku="+u.sku);}
if(b._corder){u.type=u.type||"purchase";u.name=u.name||"main";u.qty=(u.qty.length>0?u.qty:b._cquan.slice(0));u.value=(u.value.length>0?u.value:b._cprice.slice(0));u.sku=(u.sku.length>0?u.sku:b._cprod.slice(0));for(f=0;f<u.qty.length;f++){u.value[f]=u.value[f].replace(".","");g.push("qty["+f+"]="+u.qty[f],"value["+f+"]="+u.value[f],"sku["+f+"]="+u.sku[f]);}}
u.type=u.type||"install";u.user_id=u.user_id||b._ccustid;c.push("app_id="+u.appid,"user_id="+u.user_id,"type="+u.type,"name="+u.name);c=c.concat(g);u.img=new Image();u.img.src=u.base_url+c.join(u.qsp_delim);}}
try{utag.o[loader].loader.LOAD(id)}catch(e){utag.loader.LOAD(id)}})('12','square.main');}catch(e){}
