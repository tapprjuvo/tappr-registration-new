
function GpaController($scope) {

  // Letter grades and their weights for the dropdowns
  $scope.gradeopts = [
    {grade: '1', weight: 1},
    {grade: '2', weight: 2}
  ];

  // The student's courses, units and grading options.
  // This would typically come in as JSON data, retrieved via angular's $http
  $scope.schedule = [
    {
      "course_number": "BIO 1A",
      "units": "3.0",
      "grade_option": "Letter"
    }
  ];

  $scope.gpaUpdateCourse = function(course, estimated_grade) {
    // When a new selection is made, update course object on scope
    // and trigger recalculation of overall GPA
    course.estimated_grade = estimated_grade;
    $scope.gpaCalculate();
  };

  $scope.gpaCalculate = function() {
    // Recalculate GPA on every dropdown change.
    var total_units = 0;
    var total_score = 0;

    angular.forEach($scope.schedule, function(course) {

        course.score = parseFloat(course.estimated_grade, 10) * course.units;
        total_units += parseFloat(course.units, 10);
        total_score += course.score;
    });

    // The standard GPA calculation formula
    $scope.estimated_gpa = total_score / total_units;
  };
$scope.calculate_grand_total = function() {
        return $scope.calculate_tax() + $scope.invoice_sub_total();
    }
     $scope.invoice_sub_total = function() {
        var total = 0.00;
        angular.forEach($scope.invoice.items, function(item, key){
          total += (item.qty * item.cost);
        });
        return total;
    }
    $scope.calculate_tax = function() {
        return (($scope.invoice.tax * $scope.invoice_sub_total())/100);
    }
  $scope.gpaInit = function() {
    // On init, be generous... start everyone off with a 4.0
    angular.forEach($scope.schedule, function(course) {
      course.estimated_grade = 4;
    });
    $scope.gpaCalculate();
  }(); // Note "()" - makes gpaInit() a self-running function, so it fires on page load

}
